module.exports = {
  "index":{
    "dev_mode":"false;",
    "facebook_url":"https://www.facebook.com/iskconlondon",
    "twitter_url":"https://twitter.com/ISKCONLondon",
    "instagram_url":"https://www.instagram.com/iskconlondon",
    "issuu_url":"https://issuu.com/iskconlondon",
    "google_font-off":"<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>",
    "google_font":"<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>",
  },
  "styles":{
  	"font-family-off:":"'Lato', sans-serif;",
  	"font-family-body":"'Montserrat', sans-serif;",
  	"font-family-h1":"'Montserrat', sans-serif;",
  }
};