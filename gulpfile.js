var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
// var csso = require('gulp-csso');
var concatCss = require('gulp-concat-css');
// var uglifycss = require('gulp-uglifycss');
let cleanCSS = require('gulp-clean-css');
var cssnano = require('gulp-cssnano');
var replace = require('gulp-token-replace');
var rename = require('gulp-rename');

gulp.task('scripts', function() {
  return gulp.src([
  	'Web Files/templates/maya-src/js/jquery-min.js', 
  	'Web Files/templates/maya-src/js/full-versions/jquery-ui.min.js',
    'Web Files/templates/maya-src/js/modernizr-custom.js', 
  	'Web Files/templates/maya-src/js/featherlight.min.js', 
    'Web Files/templates/maya-src/js/featherlight.gallery.min.js',
    'Web Files/templates/maya-src/js/swipe.min.js',
  	// 'Web Files/templates/maya-src/js/full-versions/jquery.ui.map.full.min.js',
  	// 'Web Files/templates/maya-src/js/full-versions/easing-min.js',
  	// 'Web Files/templates/maya-src/js/full-versions/gmap.api.js',
  	// 'Web Files/templates/maya-src/js/full-versions/gmap.framework.js',
  	'Web Files/templates/maya-src/js/full-versions/jpreloader.min.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.appear.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.countTo.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.fancybox.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.fittext.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.lettering.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.mb.YTPlayer.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.mousewheel-3.0.6.pack.js',
  	'Web Files/templates/maya-src/js/full-versions/jquery.textillate.js',
  	'Web Files/templates/maya-src/js/full-versions/masonry.pkgd.min.js',
  	'Web Files/templates/maya-src/js/full-versions/modernizer-min.js',
  	'Web Files/templates/maya-src/js/full-versions/owl.carousel.min.js',
  	'Web Files/templates/maya-src/js/jflickrfeed.min.js',
    'Web Files/templates/maya-src/js/fa-v4-shims.min.js',
    'Web Files/templates/maya-src/js/fontawesome-all.min.js',
    'Web Files/templates/maya-src/js/imagesloaded.pkgd.min.js',
    'Web Files/templates/maya-src/js/isotope.pkgd.min.js',
  	'Web Files/templates/maya-src/js/scripts.js',
  	'Web Files/templates/maya-src/js/custom_scripts.js',
  	])
  	.pipe(sourcemaps.init())
    .pipe(concat('all_scripts.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('sourcemaps'))
    .pipe(gulp.dest('Web Files/templates/maya/js'))
});

gulp.task('styles-dev', function() {
  var config = require('./gulp-token-replace-config-dev.js'); 
  return gulp.src([
    'Web Files/templates/maya-src/css/styles-src.css'
    ])
    .pipe(replace({global:config}))
    .pipe(rename('./styles.css'))
    .pipe(gulp.dest('Web Files/templates/maya/css'))
});

gulp.task('styles', function() {
  var config = require('./gulp-token-replace-config-prd.js');
  return gulp.src([
    'Web Files/templates/maya-src/css/styles-src.css',
    'Web Files/templates/maya-src/css/animate.css',
    'Web Files/templates/maya-src/css/custom.css',
    'Web Files/templates/maya-src/css/btcustom.css',
    'Web Files/templates/maya-src/css/grid.css',
    'Web Files/templates/maya-src/css/icons.css',
    'Web Files/templates/maya-src/css/jquery-ui.css',
    'Web Files/templates/maya-src/css/jquery.fancybox.css',
    'Web Files/templates/maya-src/css/lightbox.css',
    'Web Files/templates/maya-src/css/owl.carousel.css',
    'Web Files/templates/maya-src/css/owl.transitions.css',
    'Web Files/templates/maya-src/css/responsive.css',
    'Web Files/templates/maya-src/css/YTPlayer.css',
    'Web Files/templates/maya-src/css/featherlight.min.css',
    'Web Files/templates/maya-src/css/featherlight.gallery.min.css',
    ])
    .pipe(sourcemaps.init())
    .pipe(replace({global:config}))
    .pipe(concatCss('all_styles.min.css'), {rebaseUrls: false})  
    .pipe(sourcemaps.write('sourcemaps'))
    .pipe(gulp.dest('Web Files/templates/maya/css'))
});

gulp.task('token-replace-dev', function(){
  var config = require('./gulp-token-replace-config-dev.js');
  return gulp.src('Web Files/templates/maya-src/index-src.php')
    .pipe(replace({global:config}))
    .pipe(rename('index.php'))
    .pipe(gulp.dest('Web Files/templates/maya'))
});

gulp.task('token-replace-prd', function(){
  var config = require('./gulp-token-replace-config-prd.js');
  return gulp.src("Web Files/templates/maya-src/index-src.php")
    .pipe(replace({global:config}))
    .pipe(rename('index.php'))
    .pipe(gulp.dest('Web Files/templates/maya'))
});

gulp.task('token-replace-prd-serverDirect', function(){
  var config = require('./gulp-token-replace-config-prd.js');
  return gulp.src("Web Files/templates/maya-src/index-src.php")
    .pipe(replace({global:config}))
    .pipe(rename('index.php'))
    .pipe(gulp.dest('../../www/templates/maya2'))
});

gulp.task('copy', function () {
  return gulp.src(['Web Files/templates/maya-src/**/*', '!Web Files/templates/maya-src/node_modules/**/*'], {base:'Web Files/templates/maya-src'})
  .pipe(gulp.dest('Web Files/templates/maya'));
});

gulp.task('copyToLocationOnSameServer', function() {
  return gulp.src('Web Files/templates/maya/**/*', {base:'Web Files/templates/maya/'})
    .pipe(gulp.dest('../../public_html/templates/maya'));
});

gulp.task('dev', ['token-replace-dev', 'styles-dev', 'copy']);
gulp.task('prd', ['token-replace-prd', 'scripts', 'styles', 'copy']);
gulp.task('prd2', ['token-replace-prd', 'scripts', 'styles', 'copy', 'copyToLocationOnSameServer']);