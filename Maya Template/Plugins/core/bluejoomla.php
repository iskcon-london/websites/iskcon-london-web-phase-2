<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 shinetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
    //no direct accees
    defined ('_JEXEC') or die('resticted aceess');

    jimport('joomla.filesystem.file');
    jimport('joomla.filesystem.folder');

    class Bluejoomla {

        private static $_instance;
        private $document;
        private $importedFiles=array();
        private $_less;

        //initialize 
        public function __construct(){

        }

        /**
        * making self object for singleton method
        * 
        */
        final public static function getInstance()
        {
            if( !self::$_instance ){
                self::$_instance = new self();
                self::getInstance()->getDocument();
                self::getInstance()->getDocument()->shinetheme = self::getInstance();
            } 
            return self::$_instance;
        }


        /**
        * Get Document
        * 
        * @param string $key
        */
        public static function getDocument($key=false)
        {
            self::getInstance()->document = JFactory::getDocument();
            $doc = self::getInstance()->document;
            if( is_string($key) ) return $doc->$key;

            return $doc;
        }
         /**
        * Get images
        * 
        * @param string $key
        */
        public static function resizeImgThumb($img,$thumb_w,$thumb_h,$folder_thumb,$key){
            if (self::getInstance()->checkImage(JPATH_ROOT.'/'.$img)){
                JFolder::create(JPATH_ROOT.'/'.$folder_thumb);
            }else{
                return '';
            }
            FileManager::load($folder_full.$img);
            FileManager::resize($thumb_w,$thumb_h,0,0,0,0);
            FileManager::save(JPATH_ROOT.'/'.$folder_thumb.$key.'.jpg');
            return $folder_thumb.$key.'.jpg';
        }
        /**
        * Get images
        * 
        * @param string $key
        */
        public static function getImages($dir = null){
            // create an array to hold directory list
            $results = array();
            if (!empty($dir)){
            
                // create a handler for the directory
                $handler = opendir(JPATH_ROOT.'/'.$dir);
            
                // open directory and walk through the filenames
                while ($file = readdir($handler)) {
            
                  // if file isn't this directory or its parent, add it to the results
                  if ($file != "." && $file != "..") {
                    if (self::getInstance()->checkImage(JPATH_ROOT.'/'.$dir.'/'.$file)){
                        $results[] = $dir.'/'.$file;
                    }
                    
                  }
            
                }
            
                // tidy up: close the handler
                closedir($handler);
                
            }
            // done!
            return $results;
        }
        /**
        * checkImage
        * 
        * @return boolean
        */
        public static function  checkImage($path) {
            if (JFile::exists($path)){
                $img_infor = getimagesize($path);
                $image_type = $img_infor[2];
                if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
                {
                    return true;
                }
            }
            
            return false;
        
        }
        /**
        * loadK2Item
        * 
        * @return object
        */
        public static function loadK2Item($id){
            
            $db = JFactory::getDBo();
            $query = $db->getQuery(true);
            $query->select('i.*,u.name as u_name')
                ->from('#__k2_items as i')
                ->innerJoin('#__users as u ON u.id=i.created_by ')
                ->where('i.id ='.(int)$id)
                ->where('i.published=1');
            
            
            return $db->setQuery($query)->loadObject();
        }
        /**
        * getFileSize
        * 
        * @return int
        */
        public static function getFileSize($id,$ext){
            
            $path = JPATH_ROOT.'/'.'/media/k2/videos/'.$id.'.'.$ext;
            if (JFile::exists($path)){
                return filesize($path);
            }
            return 0;
        }
        /**
        * Get Template name
        * 
        * @return string
        */
        public static function themeName()
        {
            //return self::getInstance()->getDocument()->template;
            return JFactory::getApplication()->getTemplate();
        }

        /**
        * Get Template name
        * @return string
        */
        public static function theme()
        {
            return self::getInstance()->themeName();
        }

        /**
        * Get or set Template param. If value not setted params get and return, 
        * else set params
        *
        * @param string $name
        * @param mixed $value
        */
        public static function Param($name=true, $value=NULL)
        {

            // if $name = true, this will return all param data
            if( is_bool($name) and $name==true ){
                return JFactory::getApplication()->getTemplate(true)->params;
            }
            // if $value = null, this will return specific param data
            if( is_null($value) ) return JFactory::getApplication()->getTemplate(true)->params->get($name);
            // if $value not = null, this will set a value in specific name.

            $data = JFactory::getApplication()->getTemplate(true)->params->get($name);

            if( is_null($data) or !isset($data) ){
                JFactory::getApplication()->getTemplate(true)->params->set($name, $value);
                return $value;
            } else {
                return $data;
            }
        }
        /**
        * Get theme path
        * 
        * @param bool $base
        * @return string
        */
        public static function themePath($base=false)
        {
            if( $base==true ) return JURI::root(true).'/templates/'.self::getInstance()->themeName();

            return  JPATH_THEMES . '/' . self::getInstance()->themeName();
        }

        /**
        * Get theme path
        * @return string
        */
        public static function themeURL()
        {
            return self::getInstance()->themePath();
        }

        /**
        * Get Base Path
        * 
        */
        public static function basePath()
        {
            return JPATH_BASE;
        } 

        /**
        * Get Base URL
        * 
        */
        public static function baseURL()
        {
            return JURI::root(true);
        }

        /**
        * Get Framework HELIX path
        * 
        */
        public static function frameworkPath($base=false)
        {
            if( $base==true ) return JURI::root(true).'/plugins/system/bluejoomla';

            return JPATH_PLUGINS . '/system/bluejoomla';
        }

        public static function pluginPath($base=false){
            return self::getInstance()->frameworkPath($base);
        }


        /**
        * Make string to slug
        * 
        * @param mixed $text
        * @return string
        */

        public static function slug($text)
        {
            return preg_replace('/[^a-z0-9_]/i','-', strtolower($text));
        }


        /**
        * Import required file/files
        * 
        * @param array | string $paths
        * @param object $helix
        * @return self
        */
        public static function Import($paths, $shinetheme=false)
        {
            if( is_array($paths) ) foreach((array) $paths as $file) self::_Import( $file );
            else self::_Import( $paths, $shinetheme );
            return self::getInstance();
        }

        /**
        * Single file import
        * 
        * @param string $path
        * @return self
        */
        private static function _Import($path, $shinetheme)
        {
            $intheme  = self::getInstance()->themePath() . '/' . $path;
            $inplugin = self::getInstance()->frameworkPath() . '/' . $path;

            if( file_exists( $intheme ) && !is_dir( $intheme ) ){
                self::getInstance()->importedFiles[] = $intheme; 
                require_once $intheme;
            } elseif( file_exists( $inplugin ) && !is_dir( $inplugin ) ){
                self::getInstance()->importedFiles[] = $inplugin; 
                require_once $inplugin;
            }
            return self::getInstance();
        }

        /**
        * Get Imported file
        * @return array
        */
        public static function getImportedFiles()
        {
            return self::getInstance()->importedFiles;
        }

        /**
        * Detact External URL
        * 
        * @param string $url
        * @return boolean
        */
        public function isExternalURL($url)
        {
            $parseurl = parse_url($url);
            $urlHost = isset($parseurl['host'])?$parseurl['host']:false;
            $currentHost = $_SERVER['HTTP_HOST'];
            $currentRemoteAddr = $_SERVER['REMOTE_ADDR'];

            if(false==$urlHost) return false;

            if( $currentHost===$urlHost or $currentRemoteAddr===$urlHost ) return false;
            else return true;
        } 


        /**
        * Add stylesheet
        * 
        * @param mixed $sources. string or array
        * @param string $seperator. default is , (comma)
        * @return self
        */
        public static function addCSS($sources, $seperator=',',$checkpath=true) {

            $srcs = array();
            if( is_string($sources) ) $sources = explode($seperator,$sources);
            if(!is_array($sources)) $sources = array($sources);

            foreach( (array) $sources as $source ) $srcs[] = trim($source);

            foreach ($srcs as $src) {

                if(self::getInstance()->isExternalURL($src)) self::getInstance()->document->addStyleSheet($src);

                if( $checkpath==false ){
                    self::getInstance()->document->addStyleSheet($src);
                    continue; 
                } 

                //cheack in template path
                if( file_exists( self::getInstance()->themePath() . '/css/'. $src)) { 
                    self::getInstance()->document->addStyleSheet( self::getInstance()->themePath(true) . '/css/' . $src );
                } 
                //if not found, then check from helix path
                elseif( file_exists( self::getInstance()->frameworkPath() . '/css/' . $src ) ) { 
                    self::getInstance()->document->addStyleSheet( self::getInstance()->frameworkPath(true) . '/css/' . $src);
                }        
            }
            return self::getInstance();
        }    

        /**
        * Add javascript
        * 
        * @param mixed $sources. string or array
        * @param string $seperator. default is , (comma)
        * @return self
        */
        public static function addJS($sources, $seperator=',', $checkpath=true) {

            $srcs = array();
            if( is_string($sources) ) $sources = explode($seperator,$sources);
            if(!is_array($sources)) $sources = array($sources);

            foreach( (array) $sources as $source ) $srcs[] = trim($source);

            foreach ($srcs as $src) {

                if(self::getInstance()->isExternalURL($src)) self::getInstance()->document->addScript($src);

                if( $checkpath==false ){
                    self::getInstance()->document->addScript($src);
                    continue; 
                } 

                //cheack in template path
                if( file_exists( self::getInstance()->themePath() . '/js/'. $src)) { 
                    self::getInstance()->document->addScript( self::getInstance()->themePath(true) . '/js/' . $src );
                } 
                //if not found, then check from helix path
                elseif( file_exists( self::getInstance()->frameworkPath() . '/js/' . $src ) ) { 
                    self::getInstance()->document->addScript( self::getInstance()->frameworkPath(true) . '/js/' . $src);
                }        
            }
            return self::getInstance();
        }    

        /**
        * Add Inline Javascript
        * 
        * @param mixed $code
        * @return self
        */
        public function addInlineJS($code){
            self::getInstance()->document->addScriptDeclaration($code);
            return self::getInstance();
        }

        /**
        * Add Inline CSS
        * 
        * @param mixed $code
        * @return self
        */
        public function addInlineCSS($code) {
            self::getInstance()->document->addStyleDeclaration($code);
            return self::getInstance();
        }


        public static function getShortcodes()
        {
            return self::getInstance()->shortcode_tags;
        }

        private static $shortcode_tags = array();
        public static function importShortCodeFiles()
        {

            $shortcodes = array();
            global $themeshortcodes;

            $pluginshortcodes = glob( self::getInstance()->pluginPath().'/shortcodes/*.php');

            foreach((array) $themeshortcodes as $value)  $shortcodes[] =  basename($value);
            foreach((array) $pluginshortcodes as $value)  $shortcodes[] =   basename($value);

            $shortcodes = array_unique($shortcodes);

            self::getInstance()->Import('core/wp_shortcodes.php', self::getInstance());

            foreach($shortcodes as $shortcode  ) self::getInstance()->Import('shortcodes/'.$shortcode);

            return self::getInstance();
        }


        private static function file($file)
        {
            // searching in template path
            if( file_exists( self::getInstance()->themePath() . '/'. $file)) { 
                return self::getInstance()->themePath() . '/'. $file;
            } 
            //if not found, then check from helix path
            elseif( file_exists( self::getInstance()->frameworkPath() . '/'. $file ) ) { 
                return self::getInstance()->frameworkPath() . '/'. $file;
            }
            return false;
        }

        private static function resetCookie($name)
        {
            if( JRequest::getVar('reset',  ''  , 'get')==1 )
                setcookie( $name, '', time() - 3600, '/');
        }


        public static function strip_shortcode($text, $tags = '', $invert = FALSE) {

            $replace = array(
                '[' => '<',
                ']' => '>'
                );
            $text = strtr($text, $replace);

            preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags); 
            $tags = array_unique($tags[1]); 

            if(is_array($tags) AND count($tags) > 0) { 
                if($invert == FALSE) { 
                    return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text); 
                } 
                else { 
                    return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text); 
                } 
            } 
            elseif($invert == FALSE) { 
                return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text); 
            } 
            return $text; 
        } 		

        /**
        * Add jQuery
        * 
        * @since    1.9.5
        * @param mixed $usecdn
        * @param mixed $forceLoad
        * @return self
        */
        public function addJQuery($usecdn=false, $forceLoad=false) {
            if (JVERSION>=3) {
                JHtml::_('jquery.framework');
                
            } else {
                $scripts = (array) array_keys( self::getInstance()->getDocument()->_scripts );
                $hasjquery=false;
                foreach($scripts as $script) {
                    if (preg_match("/\b(jquery|jquery-latest).([0-9\.min|max]+).(.js)\b/i", $script)) {
                        $hasjquery = true;
                    }  
                }

                if( $forceLoad ) $hasjquery=false;

                if( !$hasjquery ) {
                    if( $usecdn ) self::getInstance()->addJS( 'http://code.jquery.com/jquery-latest.min.js' );
                    else self::getInstance()->addJS( 'jquery.min.js' );                    
                }
            }
            self::getInstance()->addJS( 'jquery-noconflict.js' );
            return self::getInstance();
        }


        /**
        * Remove CSS
        * 
        * @param mixed $sources
        * @param mixed $seperator
        */
        public function removeJS($sources, $seperator=',') {

            $srcs = array();
            if( is_string($sources) ) $sources = explode($seperator,$sources);
            if(!is_array($sources)) $sources = array($sources);

            foreach( (array) $sources as $source ) $srcs[] = trim($source);

            $scripts = (array) array_keys( self::getInstance()->getDocument()->_scripts );
            $removedJS = array();
            foreach ($srcs as $src) {
                foreach($scripts as $script) {
                    if (preg_match("/\b".$src."\b/i", $script)) {
                        $removedJS[] = $script;
                        unset( self::getInstance()->getDocument()->_scripts[$script] );
                    }  
                }
            }
            return $removedJS;
        }


        public function removeCSS($sources, $seperator=',') {

            $srcs = array();
            if( is_string($sources) ) $sources = explode($seperator,$sources);
            if(!is_array($sources)) $sources = array($sources);

            foreach( (array) $sources as $source ) $srcs[] = trim($source);

            $scripts = (array) array_keys( self::getInstance()->getDocument()->_styleSheets );
            $removedCSS = array();
            foreach ($srcs as $src) {
                foreach($scripts as $script) {
                    if (preg_match("/\b".$src."\b/i", $script)) {
                        $removedCSS[] = $script;
                        unset( self::getInstance()->getDocument()->_styleSheets[$script] );
                    }  
                }
            }
            return $removedCSS;
        }


        // Add bootstrap	
        public function addBootstrap($responsive=true, $rtl=false) {


			// RTL enable
            if( self::getInstance()->direction()=='rtl' ) {
                self::getInstance()->addCSS('bootstrap.min.rtl.css');
				// BootStrap responsive layout rtl
                if (self::getInstance()->Param('layout_type')=='responsive') self::getInstance()->addCSS('bootstrap-responsive.min.rtl.css');
            } else {
                self::getInstance()->addCSS('bootstrap.min.css');
				// BootStrap responsive layout normal
                if (self::getInstance()->Param('layout_type')=='responsive') self::getInstance()->addCSS('bootstrap-responsive.min.css');
            }

            self::getInstance()->addCSS('font-awesome.css');
            self::getInstance()->addJQuery();
            self::getInstance()->addJS('bootstrap.min.js');

            return self::getInstance();
        }

        /**
        * 
        *
        */
        public static function selectivizr(){
            if(self::getInstance()->isIE(8))
                self::getInstance()->addJS('selectivizr-min.js');    
            return self::getInstance();    
        }

        public static function respondJS(){
            if(self::getInstance()->isIE(8))
                self::getInstance()->addJS('respond.min.js');	
            return self::getInstance();	
        }

        /**
        * Add Google Fonts
        * 
        * @param string $name. Name of font. Ex: Yanone+Kaffeesatz:400,700,300,200 or Yanone+Kaffeesatz  or Yanone Kaffeesatz
        * @param string $field. Applied selector. Ex: h1, h2, #id, .classname
        */
        public static function GoogleFont($name, $field) {

            $name = str_replace(' ', '+', $name ); 

            $font_name = explode(':', $name);
            if( is_array($font_name) ) $font_name = str_replace('+', ' ', $font_name[0] );
            else $font_name = str_replace('+', ' ', $name );

            self::getInstance()->document->addStyleSheet("http://fonts.googleapis.com/css?family=" . $name);
            $styleDeclaration = "$field{font-family:'" . $font_name . "'; -webkit-font-smoothing: subpixel-antialiased !important;}";
            self::getInstance()->document->addStyleDeclaration($styleDeclaration);
        }    

        //////////
        public static function Compression() {//compress css and js files
            if (self::getInstance()->Param('compress_css')) self::getInstance()->compressCSS();
            if (self::getInstance()->Param('compress_js')) self::getInstance()->compressJS(); 
            return self::getInstance();       
        }

        public static function compressJS() {//function to compress js files

            self::getInstance()->Import('core/classes/jsmin.php');

            $js_files = array();
            $cache_time = self::getInstance()->Param('cache_time');//Cache time in minute
            $diff=false;
            $helix_folder='helix_assets';//path of cache where to save
            $output = array();
            $md5sum = null;

            $scripts = self::getInstance()->getDocument('_scripts');//get all scripts

            foreach ($scripts as $fileSrc => $fileAttr) {//separate path from attribute
                $md5sum .= md5($fileSrc);
                $js_files[] = $fileSrc;
            }

            if (!is_writable(JPATH_CACHE)) {//check for cache path writable, if not return
                return;
            } 

            if (is_writable(JPATH_CACHE)) {//add helix_assets folder under cache directory
                if (!file_exists(JPATH_CACHE.DIRECTORY_SEPARATOR.$helix_folder)) mkdir (JPATH_CACHE.DIRECTORY_SEPARATOR.$helix_folder);
            }

            if (count($js_files) > 0) {//if any js file available
                $cache_name = md5($md5sum) . ".js";
                $cache_path = JPATH_CACHE . DIRECTORY_SEPARATOR . $helix_folder . DIRECTORY_SEPARATOR . $cache_name;


                //see if file is stale
                if (!file_exists($cache_path)) {
                    $diff=true;   
                } elseif(filesize($cache_path) == 0 || ((filemtime($cache_path) + $cache_time * 60) < time())) {
                    $diff=true; 
                }

                foreach ($js_files as $files) {
                    $external = self::getInstance()->isExternalURL($files);
                    if( $external ) continue;
                    unset(self::getInstance()->getDocument()->_scripts[$files]); //Remove js files from the header
                }

                if ($diff) {
                    $output = '';
                    foreach ($js_files as $files) {

                        $external = self::getInstance()->isExternalURL($files);
                        if( $external ) continue;

                        $filepath = self::getInstance()->realPath($files);
                        if (JFile::exists($filepath)) {
                            $js = JSMin::minify(JFile::read($filepath));//read and compress js files
                            $output .= "/*------ " . $files . " ------*/\n" . $js . "\n\n";//add file name to compressed JS
                        }
                    }
                    JFile::write($cache_path, $output);//write cache to the joomla cache directory
                }

                $cache_url =  self::getInstance()->baseURL() . "/cache/" . $helix_folder . '/' . $cache_name;//path of css cache to add as script

                self::getInstance()->document->addScript( $cache_url ); //add script to the header
            }
        }

        public static function compressCSS() {//function to compress css files

            self::getInstance()->Import('core/classes/cssmin.php');

            $css_files = array();
            $cache_time = self::getInstance()->Param('cache_time');//Cache time in minute
            $helix_folder='helix_assets';//path of cache where to save
            $output = array();
            $md5sum = null;

            $csss = self::getInstance()->getDocument('_styleSheets');//get all css

            foreach ($csss as $fileSrc => $fileAttr) {//separate path from attribute
                $md5sum .= md5($fileSrc);
                $css_files[] = $fileSrc;
            }

            if (!is_writable(JPATH_CACHE)) {//check for cache path writable, if not return
                return;
            } 

            if (is_writable(JPATH_CACHE)) {//add helix_assets folder under cache directory
                if (!file_exists(JPATH_CACHE.DIRECTORY_SEPARATOR.$helix_folder)) mkdir (JPATH_CACHE.DIRECTORY_SEPARATOR.$helix_folder);
            }

            if (count($css_files) > 0) {//if any css file available
                $cache_name = md5($md5sum) . ".css";
                $cache_path = JPATH_CACHE . DIRECTORY_SEPARATOR . $helix_folder . DIRECTORY_SEPARATOR . $cache_name;
                $diff=false;

                //see if file is stale
                if (!file_exists($cache_path)) {
                    $diff=true;   
                } elseif(filesize($cache_path) == 0 || ((filemtime($cache_path) + $cache_time * 60) < time())) {
                    $diff=true; 
                }

                foreach ($css_files as $files) {

                    $external = self::getInstance()->isExternalURL($files);
                    if( $external ) continue;

                    unset(self::getInstance()->getDocument()->_styleSheets[$files]); //Remove all css files from the header
                }

                if ($diff) {
                    $output = '';
                    foreach ($css_files as $files) {

                        $external = self::getInstance()->isExternalURL($files);
                        if( $external ) continue;

                        $filepath = self::getInstance()->realPath($files);//convert to real url

                        global $absolute_url;
                        $absolute_url = $files;//absoulte path of each css file
                        if (JFile::exists($filepath)) {
                            $css = CSSMinify::process(JFile::read($filepath));//read and compress css files

                            $css=preg_replace_callback('/url\(([^\)]*)\)/', array(self::getInstance(), 'replaceUrl'), $css);//call replaceUrl function to set absolute value to the urls

                            $output .= "/*------ " . $files . " ------*/\n" . $css . "\n\n";//add file name to compressed css
                        }
                    }
                    JFile::write($cache_path, $output);//write cache to the joomla cache directory
                }

                $cache_url = self::getInstance()->baseURL() . "/cache/" . $helix_folder . '/' . $cache_name;//path of css cache to add as stylesheet

                self::getInstance()->document->addStyleSheet( $cache_url ); //add stylesheet to the header
            }
        }

        private static function replaceUrl($matches) {//replace url with absolute path
            $url = str_replace(array('"', '\''), '', $matches[1]);
            $url = self::getInstance()->fixUrl($url);
            return "url('$url')";
        }

        private static function fixUrl($url) {
            global $absolute_url;
            $base = dirname($absolute_url);
            if (preg_match('/^(\/|http)/', $url))
                return $url;
            /*absolute or root*/
            while (preg_match('/^\.\.\//', $url)) {
                $base = dirname($base);
                $url = substr($url, 3);
            }

            $url = $base . '/' . $url;
            return $url;
        }    

        private static function realPath($strSrc) { //Real path of css or js file
            if (preg_match('/^https?\:/', $strSrc)) {
                if (!preg_match('#^' . preg_quote(JURI::base()) . '#', $strSrc)) return false; //external css
                $strSrc = str_replace(JURI::base(), '', $strSrc);
            } else {
                if (preg_match('/^\//', $strSrc)) {
                    if (!preg_match('#^' . preg_quote(JURI::base(true)) . '#', $strSrc)) return false; //same server, but outsite website
                    $strSrc = preg_replace('#^' . preg_quote(JURI::base(true)) . '#', '', $strSrc);
                }
            }
            $strSrc = str_replace('//', '/', $strSrc);
            $strSrc = preg_replace('/^\//', '', $strSrc);
            return $strSrc;
        }    

        /**
        * Detect IE Version
        *
        * @since	1.0
        */
        public static function isIE($version = false) {  
            $agent=$_SERVER['HTTP_USER_AGENT'];  
            $found = strpos($agent,'MSIE ');  
            if ($found) { 
                if ($version) {
                    $ieversion = substr(substr($agent,$found+5),0,1);   
                    if ($ieversion == $version) return true;
                    else return false;
                } else {
                    return true;
                }

            } else {
                return false;
            }
            if (stristr($agent, 'msie'.$ieversion)) return true;
            return false;        
        }
    }

class FileManager{
    
    function __construct(){
        
    }
    //get list files 
     public static function read_File($direction,$root){
        
        $dir = opendir($root.$direction);
        $files =  array();
        while (($file = readdir($dir)) !== false){
            
            $files[] = $file;
        }
        closedir($dir);
        return $files;
    }
    // read files information
     public static function read_infor_file($direction, $root){
        
        $fileList = $this->read_File($direction,$root);
        $infor = array();
        foreach($fileList as $k=>$file){
            
            $infor[$k]['root'] = $root;
            $infor[$k]['direction'] = $direction;
            $infor[$k]['name'] = $file;
            $infor[$k]['type'] = filetype($root.$direction.'\\'.$file);
            $infor[$k]['modified'] = date('F d i H:i:s',filemtime($root.$direction.'\\'.$file));
            if (is_file($direction.'\\'.$file))
                $infor[$k]['size'] = filesize($root.$direction.'/'.$file).' Byte';
            else $infor[$k]['size'] = '-';
        }
        return $infor;
    }
    
    public static $image;
    public static $image_type;
    public static $width;
    public static $height;
     public static function load($filename) {
        
        $image_info = getimagesize($filename);
        FileManager::$image_type = $image_info[2];
        FileManager::$width = $image_info[0];
        FileManager::$height = $image_info[1];
        if( FileManager::$image_type == IMAGETYPE_JPEG ) {
            
            FileManager::$image = imagecreatefromjpeg($filename);
            
        } elseif( FileManager::$image_type == IMAGETYPE_GIF ) {
            
            FileManager::$image = imagecreatefromgif($filename);
        } elseif( FileManager::$image_type == IMAGETYPE_PNG ) {
            
            FileManager::$image = imagecreatefrompng($filename);
        }
    }
     public static function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        
        if( $image_type == IMAGETYPE_JPEG ) {
            
            imagejpeg(FileManager::$image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            
            imagegif(FileManager::$image,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            
            imagepng(FileManager::$image,$filename);
        }
        if( $permissions != null) {
            
            chmod($filename,$permissions);
        }
    }
     public static function getWidth() {
        
        return FileManager::$width;
    }
     public static function getHeight() {
        
        return FileManager::$height;
    }
    public static function resize($width,$height, $dst_x, $dst_y, $src_x, $src_y) {
        
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, FileManager::$image, $dst_x, $dst_y, $src_x, $src_y, $width, $height, FileManager::getWidth(), FileManager::getHeight());
        FileManager::$image = $new_image;
    }
}   
?>