<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[plans]
if(!function_exists('plan_func')) {
	$planArray = array();
	function plan_func( $atts, $content="" ){
		global $planArray;
		extract(shortcode_atts(array(
			   'title' => 'Price table',
              'short_desc' => '',
              'currency' => '$',
              'unit' => 'Per month'            
		 ), $atts));
		$features = array(
            '[plan_features]'=>'',
            '[/plan_features]'=>'',
            '[plan_feature_item]'=>'<li>',
            '[/plan_feature_item]'=>'</li>'
		);
		$html = '<div class="container"><div class="row">';
		do_shortcode( $content );
		//item
        if (count($planArray) == 4) $class = ' span3 '; 
        if (count($planArray) == 3) $class = ' span4 ';
        if (count($planArray) == 2) $class = ' span6 ';
        if (count($planArray) == 6) $class = ' span2 ';
		foreach ($planArray as $val) {
			$html .='<div class="pricingtable '.$val["type"].' '.$class.'" >
                            <div class="top">
                            <h2>'.$val["title"].'</h2>
                            </div>
                            <ul>
                                '.strtr($val["content"], $features).'
                            </ul>
                            <hr>                            
                            <h1><sup>'.$currency.'</sup>'.$val["price"].'</h1>
                            <p>'.$unit.'</p>
                            <a class="my-btn btn-orange btn-large" href="'.$val['link'].'">'.$val['text'].'</a> 
                     </div>';
		}
		
		$html .=' </div></div>';
		$planArray = array();	
		return $html;
	}
	
	add_shortcode( 'plan', 'plan_func' );
		
	//plan Item
	function plan_item_func( $atts, $content="" ){
		global $planArray;
        extract(shortcode_atts(array(
			   'title' =>'Default',
              'price' =>'',
              'buy_link' =>'',
              'buy_text' =>'',
              'type'=>''              
		 ), $atts));
		$planArray[] = array(
            'content'=>$content,
            'title'=>$title,
            'price'=>$price,
            'link'=>$buy_link,
            'text'=>$buy_text,
            'type'=>$type 
        );
	}

	add_shortcode( 'plan_item', 'plan_item_func' );	
    
}