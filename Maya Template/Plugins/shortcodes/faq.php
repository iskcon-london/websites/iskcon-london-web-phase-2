<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[FAQ]
if(!function_exists('faq_sc')) {
	$faqArray = array();
	function faq_sc( $atts, $content="" ){
		global $faqArray;
		
		$params = shortcode_atts(array(
               'id' => 'faq',
               'title'=>'',
               'desc' =>''
		 ), $atts);
		
		do_shortcode( $content );
        $html = '<div class="section-content faqs-section transparent-back">
				<div class="title-section">
					<div>
						<h1>Frequently Asked Questions</h1>
						<p>Vestibulum commodo felis quis tortor.</p>
					</div>
				</div>
				<div class="toggle-section">
					<div class="accordion-box">';
                        ?>
                        <?php 
                        foreach($faqArray as $key=>$faq):
                        $active = '';
                        if ($key == 0) $active ='active';
						$html .= '<div class="accord-elem '.$active.'">
							<div class="accord-title">
								<h2>'.$faq['title'].'</h2>
								<a class="accord-link" href="#"></a>
							</div>
							<div class="accord-content">
								'.do_shortcode($faq['content']).'
							</div>
						</div> ';
                         endforeach; ?>
					
        <?php
	   $html .= '</div>
				</div>
			</div>';
		$faqArray = array();	
		return $html;
	}
	
	add_shortcode( 'faq', 'faq_sc' );
		
	//faq Items
	function faq_item_sc( $atts, $content="" ){
		global $faqArray;
		$faqArray[] = array('title'=>$atts['title'], 'content'=>$content);
	}

	add_shortcode( 'faq_item', 'faq_item_sc' );			
}