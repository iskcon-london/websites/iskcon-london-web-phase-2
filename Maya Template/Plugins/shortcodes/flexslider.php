<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[flexsliders]
if(!function_exists('flexslider_func')) {
	$flexsliderArray = array();
	function flexslider_func( $atts, $content="" ){
		global $flexsliderArray;
		
		$params = shortcode_atts(array(
			  'title' => '',
              'short_desc' => ''
		 ), $atts);
		$desc = array(
			'[desc]'=>'<p>',
            '[/desc]'=>'</p>'
		);
		do_shortcode( $content );
		$html = '<div class="flex-wrap">
                    <div class="flexslider">
                        <ul class="slides">';
		
		//item
		foreach ($flexsliderArray as $flex) {
			$html .='<li><div class="container">';
            $html .='<img src="'.JUri::root().$flex['image'].'" alt="" class="img-rounded" />
                    <div class="flex-caption">
                    	<div>
                            <h2>'.$flex['title'].'</h2>
                            '.do_shortcode( strtr($flex['content'], $desc) ).'
                        </div>
                    </div>';
            $html .='</div></li>';
		}
		
		$html .='</ul></div></div>';
		$flexsliderArray = array();	
		return $html;
	}
	
	add_shortcode( 'flexslider', 'flexslider_func' );
		
	//flexslider Item
	function flexslider_item_func( $atts, $content="" ){
		global $flexsliderArray;
        extract(shortcode_atts(array(
			   'title' =>'Flexslider',
               'image' =>''            
		 ), $atts));
		$flexsliderArray[] = array(
            'content'=>$content,
            'title'=>$title,
            'image'=>$image 
        );
	}

	add_shortcode( 'flexslider_item', 'flexslider_item_func' );		
}