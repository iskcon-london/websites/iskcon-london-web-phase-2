<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[testimonials]
if(!function_exists('testimonial_func')) {
	$testimonialArray = array();
	function testimonial_func( $atts, $content="" ){
		global $testimonialArray;
		extract(shortcode_atts(array(
			   'title' => '',  
               'id' => '',
               'desc' =>'',
               'background_image' =>''         
		 ), $atts));
         $bg_image = '';
        if (!empty($background_image)) 
            $bg_image= 'style="background-image: url('.JUri::root().$background_image.');"';
		$html = '<div id="quotes" class="section">

                <div class="quote-content">
                    
                    <div class="parallax" '.$bg_image.'>
                    
                        <div class="quote-layer padd-y-50 bg-nd-alpha">
                        
                            <div id="quote-carousel">';
		do_shortcode( $content );
        
		foreach ($testimonialArray as $key=>$val) {
		   $item_bg = '';
           $class = '';
           $trans_top = '';
           $trans_bottom = ''; 
           if (($key % 2 == 0)){ $class = 'boxed'; $trans_top = 'transit-top';$trans_bottom = 'transit-bottom';}
           else{ $class = 'quote-message'; $trans_top = '';$trans_bottom = '';}
            if (!empty($val['avatar'])) 
                $item_bg= 'style="background-image: url('.JUri::root().$val['avatar'].');"';
			$html .='<div class="'.$class.'">
                    
                    	<div class="quote-thumb " '.$item_bg.'></div>
                    
                        <div class="quote-exc padd-x-25 ">'.$val['desc'].'</div>
                    
                        <div class="line-center brd-gr2-clr "></div>
                                                    
                        <div class="quote-author padd-x-25 ">'.$val['title'].'</div>
                        
                    </div>';
                    
		}
		$html .='</div></div></div></div></div>';
    	
	
		$testimonialArray = array();	
		return $html;
	}
	
	add_shortcode( 'testimonial', 'testimonial_func' );
		
	//testimonial Item
	function testimonial_item_func( $atts, $content="" ){
		global $testimonialArray;
        extract(shortcode_atts(array(
			   'title' =>'',
              'avatar' =>'',
              'desc' =>''          
		 ), $atts));
		$testimonialArray[] = array(
            'content'=>$content,
            'title'=>$title,
            'avatar'=>$avatar,
            'desc' =>$desc
        );
	}

	add_shortcode( 'testimonial_item', 'testimonial_item_func' );	
    
}