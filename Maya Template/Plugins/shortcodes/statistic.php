<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[statistics]
if(!function_exists('statistic_func')) {
	$statisticArray = array();
	function statistic_func( $atts, $content="" ){
		global $statisticArray;
		extract(shortcode_atts(array(
			   'title' => '',  
               'id' => '',
               'desc' =>'',
               'background_image' =>''         
		 ), $atts));
         $bg_image = '';
        if (!empty($background_image)) 
            $bg_image= 'style="background-image: url('.JUri::root().$background_image.');"';
		$html = '<div id="counters" class="section">
                <div class="count-content">                
                	<div class="parallax" '.$bg_image.'>                        
                        <div class="count-layer padd-y-50 bg-wh-alpha">                        
                            <div class="boxed">';
		do_shortcode( $content );
        
		//item
        if (count($statisticArray) == 4) $class = ' col-1-4 '; 
        if (count($statisticArray) == 3) $class = ' col-1-3 ';
        if (count($statisticArray) == 2) $class = ' col-1-2 ';
        if (count($statisticArray) == 5) $class = ' col-1-5 ';
		foreach ($statisticArray as $key=>$val) {
			$html .='<div class="'.$class.' transit-fade">
                        <div class="count-box padd-25">
                            <div class="count-icon"><i class="'.$val['icon_name'].' nd-clr"></i></div>
                            <div class="line-center brd-gr3-clr brd-nd-clr"></div>
                            <div class="count-value nd-clr nd-clr" data-speed="1000" data-from="0" data-to="'.$val['percent'].'">0</div>
                            <div class="count-title nd-clr">'.$val['title'].'</div>
                        </div>
                    </div>
                    ';
                    
		}
		$html .='<div class="clear"></div> </div></div></div></div></div>';
    	
	
		$statisticArray = array();	
		return $html;
	}
	
	add_shortcode( 'statistic', 'statistic_func' );
		
	//statistic Item
	function statistic_item_func( $atts, $content="" ){
		global $statisticArray;
        extract(shortcode_atts(array(
			   'title' =>'',
              'percent' =>'',
              'icon_name' =>'' ,
              'desc' =>''          
		 ), $atts));
		$statisticArray[] = array(
            'content'=>$content,
            'title'=>$title,
            'percent'=>$percent,
            'icon_name'=>$icon_name,
            'desc' =>$desc
        );
	}

	add_shortcode( 'statistic_item', 'statistic_item_func' );	
    
}