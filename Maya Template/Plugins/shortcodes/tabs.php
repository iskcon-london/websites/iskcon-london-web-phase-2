<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');
	
//[Tab]
if(!function_exists('tab_sc')) {
	$tabArray = array();
	function tab_sc( $atts, $content="" ){
		global $tabArray;
		
		$params = shortcode_atts(array(
			  'id' => 'tab',
			  'class'=>'',
              'title'=>''
		 ), $atts);
		
		do_shortcode( $content );
        $html = '<h3>'.$params['title'].'</h3>
                    
                        <div id="ui-tabs">';
	
		//Tab Title
		$html .='<ul>';
		foreach ($tabArray as $key=>$tab) {
			$html .='<li class="brd-fs-clr"><a href="#'. $params['id']  . '-' . $key.'" data-curtain="false"><strong>'. $tab['title'] .'</strong></a></li>
                  ';
		     
        }
		$html .='</ul>';
		
		//Tab Content
		foreach ($tabArray as $key=>$tab) {
			$html .='<div id="'. $params['id'] . '-' . $key.'" class="brd-gr2-clr">
                 ' . do_shortcode($tab['content']) .'   
                </div>';
         }
        $html .= '</div>';
		
		
		$tabArray = array();
		return $html;
	}
	add_shortcode( 'tab', 'tab_sc' );
	
	//Tab Items
	function tab_item_sc( $atts, $content="" ){
		global $tabArray;
		$tabArray[] = array('title'=>$atts['title'],
              'content'=>$content);
	}

	add_shortcode( 'tab_item', 'tab_item_sc' );	
    
    function st_module_func($atts, $content="" ){
                $mod_position = $content;
                jimport( 'joomla.application.module.helper' );
                $new_modules = JModuleHelper::getModules($mod_position);
                $attribs['style'] = 'xhtml';
				if (count(JModuleHelper::getModules($mod_position))){
				    $mod_content = JModuleHelper::renderModule($new_modules[0], $attribs);
                    return (string)$mod_content;
				}
                       
    }
    add_shortcode( 'st_module', 'st_module_func' );	
}