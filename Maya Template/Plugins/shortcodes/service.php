<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[services]
if(!function_exists('service_func')) {
	$serviceArray = array();
	function service_func( $atts, $content="" ){
		global $serviceArray;
		
		$params = shortcode_atts(array(
			  'title' => '',
              'desc' => '',
              'background_image'=> '',
              'style' => '1'
		 ), $atts);
         $bg = '';
         $style = $params['style'];
         if (empty($params['title'])){
            $padding_title = '';
         }else{$padding_title = 'padd-y-75';}
         
        if ($style === '1'){
            
    		if (!empty($params['background_image'])){
    		  $bg = "background-image: url('".JUri::root().$params['background_image']." ')";
    		}
    		do_shortcode( $content );
    		$html = '<div id="overview" class="section">';
    	
            $html .=  '<div class="header-section '.$padding_title.' ">';
            if (!empty($params['title'])){
                $html .=  '<div class="title-section padd-x-25 nd-clr transit-words">'.$params['title'].'</div>';
            }
            if(!empty($params['desc'])){
                $html .=  ' <div class="subtitle-section padd-x-25">'.htmlspecialchars_decode($params['desc']).'</div>
                        ';
                
            }
            
                       
             $html .=  '</div>
                     
                    <div class="overw-content">
                        
                        <div class="parallax" style="'.$bg.'">	
                        
                            <div class="overw-list padd-y-75 bg-fs-alpha">
                               
                                <div class="boxed">';
    		
    		//item
    		foreach ($serviceArray as $val) {
    			$html .='<div class="col-1-4">
                            <div class="overw-box padd-25 transit-left">
                                <div class="overw-icon fs-clr-hov transit"><i class="'.$val['icon_name'].'"></i></div>
                                <div class="overw-title marg-y-25">'.$val['title'].'</div>
                                <div class="line-center brd-gr2-clr"></div>
                                <div class="overw-exc">'.$val['desc'].'</div>
                            </div>
                        </div>
                        ';
                        
                       
    		}
    		
    		$html .='<div class="clear"></div></div></div></div></div></div>';
        }elseif($style === '2'){
            if (!empty($params['background_image'])){
    		  $bg = "background-image: url('".JUri::root().$params['background_image']." ')";
    		}
    		do_shortcode( $content );
    		$html = '<div id="features" class="section">

                    <div class="header-section '.$padding_title.'">
                        
                        <div class="title-section padd-x-25 gr3-clr transit-words">'.$params['title'].'</div>
                        <div class="subtitle-section padd-x-25">'.htmlspecialchars_decode($params['desc']).'</div>
                        
                    </div>
                        
                    <div class="feat-content">
                    
                        <div class="feat-list padd-y-50">
                        
                            <div class="boxed"> ';
    		
    		//item
    		foreach ($serviceArray as $val) {
    			$html .='<div class="col-1-3 transit-bottom" data-delay="0">
                            <div class="feat-box marg-25">
                                <div class="feat-icon fs-clr brd-fs-clr bg-fs-clr-hov transit"><i class="'.$val['icon_name'].'"></i></div>
                                <div class="feat-title fs-clr">'.$val['title'].'</div>
                                <div class="line-center brd-gr2-clr"></div>
                                <div class="feat-exc">'.$val['desc'].'</div>
                            </div>
                        </div>
                            ';
                        
                       
    		}
    		
    		$html .='<div class="clear"></div></div></div></div></div>';
        }elseif($style === '3'){
            if (!empty($params['background_image'])){
    		  $bg = "background-image: url('".JUri::root().$params['background_image']." ')";
    		}
    		do_shortcode( $content );
    		$html = '<div  class="section">';
            if (!empty($params['title'])){
             $html .= '<div class="header-section '.$padding_title.'">';
                        
             $html .= '<div class="title-section padd-x-25 gr3-clr transit-words">'.$params['title'].'</div>';
             $html .= '<div class="subtitle-section padd-x-25">'.htmlspecialchars_decode($params['desc']).'</div>';
              $html .= '          
                    </div>';
             }           
              $html .= '<div class="serv-list parallax" style="'.$bg.'">
        
                    <div class="serv-layer padd-y-25 bg-bk-alpha">
                    
                        <div class="boxed">
                            <div id="serv-carousel">';
    		
    		//item
    		foreach ($serviceArray as $val) {
    			$html .='<div >
                            <div class="serv-box padd-25 marg-25 brd-wh-clr">
                                <div class="serv-icon wh-clr"><i class="'.$val['icon_name'].'"></i></div>
                                <div class="serv-title wh-clr">'.$val['title'].'</div>
                                <div class="line-center brd-wh-clr"></div>
                                <div class="serv-exc wh-clr">'.$val['desc'].'</div>
                            </div>
                        </div>';
                        
                       
    		}
    		
    		$html .='<div class="clear"></div></div></div></div></div></div>';
        }
		$serviceArray = array();	
		return $html;
	}
	
	add_shortcode( 'service', 'service_func' );
		
	//service Item
	function service_item_func( $atts, $content="" ){
		global $serviceArray;
        extract(shortcode_atts(array(
			   'title' =>'',
			   'icon_name' =>'',
               'desc'=>''           
		 ), $atts));
		$serviceArray[] = array(
            'content'=>$content,
            'title'=>$title,
            'icon_name' =>$icon_name,
            'desc'=>$desc   
        );
	}

	add_shortcode( 'service_item', 'service_item_func' );		
}