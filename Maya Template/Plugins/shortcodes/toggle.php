<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[toggle]
if(!function_exists('toggle_sc')) {
	$toggleArray = array();
	function toggle_sc( $atts, $content="" ){
		global $toggleArray;
		
		$params = shortcode_atts(array(
               'id' => 'toggle',
               'title'=>'',
               'desc' =>''
		 ), $atts);
		
		do_shortcode( $content );
        $html = '';
		$html .= '<h3>'.$params['title'].'</h3>';
		//toggles
		foreach ($toggleArray as $key=>$toggle) {
		  
			$html .= '<div class="ui-toggle">
                            <div class="ui-toggle-header brd-fs-clr"><strong>'.$toggle['title'].'</strong>
                                <div class="ui-toggle-content">
                                    '.do_shortcode($toggle['content']).'
                                </div>
                            </div>
                        </div>
            ';
            
			
		}
		
		
		$html .='';
        
	
		$toggleArray = array();	
		return $html;
	}
	
	add_shortcode( 'toggle', 'toggle_sc' );
		
	//toggle Items
	function toggle_item_sc( $atts, $content="" ){
		global $toggleArray;
		$toggleArray[] = array('title'=>$atts['title'], 'content'=>$content);
	}

	add_shortcode( 'toggle_item', 'toggle_item_sc' );			
}