<?php
/**
 * @author Bluejoomla
 * @date: 01-04-2014
 *
 * @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[Accordion]
if(!function_exists('accordion_sc')) {
	$accordionArray = array();
	function accordion_sc( $atts, $content="" ){
		global $accordionArray;

		$params = shortcode_atts(array(
			'id' => 'accordion',
			'title'=>'',
			'desc' =>''
		), $atts);

		do_shortcode( $content );
		$html = '';
		$html .= '<h3>'.$params['title'].'</h3>

                        <div id="ui-accordion">';
		//Accordions
		foreach ($accordionArray as $key=>$accordion) {

			$html .= '<strong class="brd-fs-clr">'.$accordion['title'].'</strong>
                    <div class="brd-fs-clr">'.do_shortcode($accordion['content']).'</div>
            ';


		}


		$html .='</div>';


		$accordionArray = array();
		return $html;
	}

	add_shortcode( 'accordion', 'accordion_sc' );

	//Accordion Items
	function accordion_item_sc( $atts, $content="" ){
		global $accordionArray;
		$accordionArray[] = array('title'=>$atts['title'], 'content'=>$content);
	}

	add_shortcode( 'accordion_item', 'accordion_item_sc' );
}