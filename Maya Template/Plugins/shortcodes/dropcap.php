<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[dropcap]
if(!function_exists('dropcap_sc')){
	function dropcap_sc($atts, $content=''){
        return '<span class="dropcap">'.$content.'</span>';
    }
	add_shortcode('dropcap','dropcap_sc');

}
