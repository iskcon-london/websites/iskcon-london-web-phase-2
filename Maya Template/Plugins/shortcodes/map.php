<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');
	
//[Map]
if(!function_exists('map_func')) {
	function map_func( $atts, $content="" ){
		
			extract(shortcode_atts(array(
				  'address' => '',
				  'width'=>'100%',
				  'height' => '400',
				  'zoom' => 14
			 ), $atts));
            $replaces = array(' '=>'+');
			$address = strtr($address,$replaces);
            $prepAddr = str_replace(' ','+',$address);
 
            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
             
            $output= json_decode($geocode);
            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
			ob_start();
			?>
			
			
			<div class="google-maps">
            </div>
            <script type='text/javascript' src="<?php echo JURI::base(true); ?>/templates/coyote/js/vendor/jquery-1.8.3.min.js"></script>

			<script type="text/javascript">
            
        	jQuery('.google-maps').gmap({
        		zoom: <?php echo $zoom ?>,
        		center: <?php echo "'".$lat.', '.$long."'"; ?>
        	});
            </script>	
			<?php
			
			$data = ob_get_clean();
			return $data;
	}
	add_shortcode( 'stmap', 'map_func' );
}