<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[intros]
if(!function_exists('intro_func')) {
	$introArray = array();
	function intro_func( $atts, $content="" ){
		global $introArray;
		
		$params = shortcode_atts(array(
			  'title' => '',
              'short_desc' => ''
		 ), $atts);
		
		do_shortcode( $content );
		$html = '<div id="intro-slideshow">';
		
		//item
		foreach ($introArray as $val) {
		  $bg = '';
		  if (!empty($val['image_path'])){
    		  $bg = "background-image: url('".JUri::root().$val['image_path']."')";
    		}
			$html .='
                    <div>
       
                        <span class="item-image" style="'.$bg.'"></span>';
			if (!empty($val['title'])):
            $html .='            
                        <div class="intro-message">
                        
                            <div class="intro-title padd-x-50">'.$val['title'].'</div>
                            <div class="intro-subtitle marg-x-50 bg-fs-clr">'.$val['desc'].'</div>
                                               
                        </div>  ';
			endif;
            $html .='            
                   </div>';
                   
		}
		
		$html .='</div>';
		$introArray = array();	
		return $html;
	}
	
	add_shortcode( 'intro', 'intro_func' );
		
	//intro Item
	function intro_item_func( $atts, $content="" ){
		global $introArray;
        extract(shortcode_atts(array(
			   'title' =>'intro',
               'image' =>'',
               'desc'=>'',
               'image_path'=>''           
		 ), $atts));
		$introArray[] = array(
            'content'=>$content,
            'title'=>$title,
            'image'=>$image ,
            'desc'=>$desc,
               'image_path'=>$image_path    
        );
	}

	add_shortcode( 'intro_item', 'intro_item_func' );		
}