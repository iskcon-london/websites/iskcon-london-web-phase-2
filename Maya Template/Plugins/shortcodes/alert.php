<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[alert]
if(!function_exists('alert_sc')){
	function alert_sc($atts, $content=''){
		 extract(shortcode_atts(array(
        "type" => '',
        "close" => true
     ), $atts));
     $close_html = '';
     if ($close) $close_html = '<a class="close" data-dismiss="alert">×</a>';
     return '<div class="alert alert-' .strtolower($type) . '">'.$close_html.'<h4 style="text-transform: capitalize;">'.$type.'</h4>' . do_shortcode( $content ) . '</div>';
	}
	add_shortcode('alert','alert_sc');
}

