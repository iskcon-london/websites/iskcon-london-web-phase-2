<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[skill]
if(!function_exists('skill_sc')) {
	$skillArray = array();
	function skill_sc( $atts, $content="" ){
		global $skillArray;
		
		$params = shortcode_atts(array(
			  'id' => 'skill1',
              'title'=>'',
              'percent'=>0
		 ), $atts);
         $html = '<p>'.$params['title'].'</p>
                    <div class="meter nostrips">
					<p style="width: '.$params['percent'].'"><span>'.$params['percent'].'</span></p>
				</div>';
		return $html;
	}
	
	add_shortcode( 'skill', 'skill_sc' );
				
}