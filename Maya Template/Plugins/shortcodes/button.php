<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[button type="primary" size="large" link="" trget=""]
if(!function_exists('button_func')){
	function button_func($atts, $content='') {
		extract(shortcode_atts(array(
					"type" => '',
					"size" => '',
					"link" => '',
                    "color" =>'',
					"target"=>'',
                    "label"=>''
				), $atts));
		return '<a href="' . $link . '" class="button ' . $type . ' ' . $size .' '.$color. '">' .  $label  . '</a>';
	}
	add_shortcode('button', 'button_func');
}