<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/
//no direct accees
defined ('_JEXEC') or die('resticted aceess');

//[Icon]
if(!function_exists('icon_sc')) {

	function icon_sc( $atts, $content="" ) {
	
		extract(shortcode_atts(array(
			   'name' => '',
			   'class' =>""             
		 ), $atts));
		return '<i class="fa fa-' .$name.' ' .$name. ' ' . $class . '" ></i>' . $content;
	 
	}
		
	add_shortcode( 'icon', 'icon_sc' );
}