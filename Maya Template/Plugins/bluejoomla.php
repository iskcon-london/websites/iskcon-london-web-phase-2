<?php
/**
* @author Bluejoomla 
* @date: 01-04-2014
*
* @copyright  Copyright (C) 2013 cmsbluetheme.com . All rights reserved.
* @license    GNU General Public License version 2 or later; see LICENSE
*/

    //no direct accees
    defined ('_JEXEC') or die ('resticted aceess');

    jimport( 'joomla.event.plugin' );

    class  plgSystemBluejoomla extends JPlugin
    {
        function onAfterInitialise()
        {
            $shinetheme_path = JPATH_PLUGINS.'/system/bluejoomla/core/bluejoomla.php';
            if (file_exists($shinetheme_path)) {
                require_once($shinetheme_path);
                Bluejoomla::getInstance();
            } else {
                echo JText::_('Bluejoomla framework not found.');
                jexit();
            }
        }

        function onAfterRoute()
        {

            $app = JFactory::getApplication();
            if('com_search' == JRequest::getCMD('option') and !$app->isAdmin()) {
               require_once(dirname(__FILE__) .'/html/com_search/view.html.php');
            }
        }


        function onAfterRender()
        {
            if(  !JFactory::getApplication()->isAdmin() ){

                $document = JFactory::getDocument();
                $type = $document->getType();

                if($type=='html') {
                   // Bluejoomla::compressCSS();
                    //Bluejoomla::compressJS();
                    $oldhead = $document->getHeadData();  // old head
            
                    $data =  JResponse::getBody();
                    Bluejoomla::getInstance()->importShortCodeFiles();
                    $data = shortcode_unautop($data);
                    $data = do_shortcode($data); 
                    $newhead = $document->getHeadData();  // new head
                    $scripts = (array) array_diff_key($newhead['scripts'], $oldhead['scripts']);
                    $styles  = (array) array_diff_key($newhead['styleSheets'], $oldhead['styleSheets']);
                    $new_head_data = '';
                    foreach ($scripts as $key => $type)
                        $new_head_data .= '<script type="' . $type['mime'] . '" src="' . $key . '"></script>';

                    foreach ($styles as $key => $type)
                        $new_head_data .=  '<link rel="stylesheet" href="' . $key . '" />';

                    $data = str_replace('</head>', $new_head_data . "\n</head>", $data);
                    JResponse::setBody($data);
                }
            }
        }
        function onContentPrepareForm($form, $data)
        {
            if ($form->getName()=='com_menus.item') //Add bluejoomla menu params to the menu item
            {
                JHtml::_('behavior.framework');
                $doc = JFactory::getDocument();

                JForm::addFormPath(JPATH_PLUGINS.'/system/bluejoomla/fields');
                $form->loadFile('params', false);


                // 2.5
                if (JVERSION < 3) {
                    $plg_path = JURI::root(true).'/plugins/system/bluejoomla/js/admin/menuscript.25.js';
                } else {
                    $plg_path = JURI::root(true).'/plugins/system/bluejoomla/js/admin/menuscript.30.js';//	for joomla 3.0		
                }
                $doc->addScript($plg_path);
            }
}

}