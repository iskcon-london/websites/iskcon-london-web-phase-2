
jQuery.get( "https://www.iskcon-london.org/api/daily-darshan.php", { action: "shrine_updates_dev"})
  .success(function( data ) {
    if(data.status == 'success'){
        console.log('Sucess')
        console.log(data);
    }
    else if(data.status == 'error'){
        console.log("Error");
    }
  })
  .error(function(x,e) {
    if (x.status==0) {
        alert('You are offline!!\n Please Check Your Network.');
    } else if(x.status==404) {
        alert('Requested URL not found.');
    } else if(x.status==500) {
        alert('Internel Server Error.');
    } else if(e=='parsererror') {
        alert('Error.\nParsing JSON Request failed.');
    } else if(e=='timeout'){
        alert('Request Time out.');
    } else {
        alert('Unknown Error.\n'+x.responseText);
    }
});