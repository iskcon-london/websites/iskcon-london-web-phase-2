<?php

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
  header('Access-Control-Allow-Headers: token, Content-Type');
  header('Access-Control-Max-Age: 1728000');
  header('Content-Length: 0');
  header('Content-Type: text/plain');
  die();
}

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');



function dirToArray($dir) {
    $result = array();
 
    $cdir = scandir($dir);
    foreach ($cdir as $key => $value)
    {
       if (!in_array($value,array(".","..")))
       {
          if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
          {
             $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
          }
          else
          {
             $result[] = $value;
          }
       }
    }
   
    return $result;
 } 

  $possible_url = array("shrine_updates", "shrine_updates_dev");

  $value = "An error has occurred";

  if (isset($_GET["action"]) && in_array($_GET["action"], $possible_url)) {
      switch ($_GET["action"]) {
          case "shrine_updates":
              $value = dirToArray('../images/com_droppics/20');
              $outfit_details = $file = file_get_contents('../images/com_droppics/20/outfit-details.txt', FILE_USE_INCLUDE_PATH);
              $value['shrine_outfit_text']=$outfit_details;
              $value['base_path_url']='https://www.iskcon-london.org/images/com_droppics/20/';
              $value['status']='success';
              break;
          case "shrine_updates_dev":
              $value['shrine_outfit_text']="whioooo";
              $value = getDirectoryTree('../images/com_droppics/20', 'jpg');
              break;

      }
  }

  exit(json_encode($value));

?>