<?php
header("Content-type: text/css; charset=utf-8");

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}
$fs_clr = '#'.$_GET['group1'];
$fs_alpha = 'rgba('.implode(",", hex2rgb($fs_clr)).', 0.7)';
$nd_clr = '#'.$_GET['group2'];
$nd_alpha = 'rgba('.implode(",", hex2rgb($nd_clr)).', 0.7)';
?>
/* CUSTOM COLORS */
    	
.fs-clr { color: <?php echo $fs_clr; ?>; } 
.fs-clr-hov:hover { color: <?php echo $fs_clr; ?>; } 
.bg-fs-clr { background-color: <?php echo $fs_clr; ?>; }
.bg-fs-clr-hov:hover { background-color: <?php echo $fs_clr; ?>; } 
.bg-fs-alpha { background-color: <?php echo $fs_alpha; ?>; }
.brd-fs-clr { border-color: <?php echo $fs_clr; ?>; }
.brd-fs-clr-hov:hover { border-color: <?php echo $fs_clr; ?>; }
	
.nd-clr { color: <?php echo $nd_clr; ?>; } 
.nd-clr-hov:hover { color: <?php echo $nd_clr; ?>; } 
.bg-nd-clr { background-color: <?php echo $nd_clr; ?>; }
.bg-nd-clr-hov:hover { background-color: <?php echo $nd_clr; ?>; }
.bg-nd-alpha { background-color: <?php echo $nd_alpha; ?>; }
.brd-nd-clr { border-color: <?php echo $nd_clr; ?>; }
.brd-nd-clr-hov:hover { border-color: <?php echo $fs_clr; ?>; }
	
/* Preloader */

#jpreBar {
	background: <?php echo $fs_clr; ?>;
}

#jprePercentage {
	border-color: <?php echo $fs_clr; ?>;
	color: <?php echo $fs_clr; ?>;
}

/* Standard Menu/Submenu */
	
.main-menu li:hover a  {
	color: <?php echo $nd_clr; ?>;
}

.header-dark .sub-menu li a {
	background-color: <?php echo $nd_clr; ?> !important;
}

.header-dark .sub-menu li a:hover {
	background-color: #fff !important;
	color: <?php echo $nd_clr; ?>
}

.sub-menu li:hover a {
	color: <?php echo $nd_clr; ?> !important;
}

/* Sticky Menu/Submenu */

.sticky .main-menu li, .sticky .main-menu li a {
	color: <?php echo $nd_clr; ?>;
}

.sticky .main-menu li:hover a {
	background-color: <?php echo $nd_clr; ?>;
}

.sticky .sub-menu li a {
	background-color: <?php echo $fs_clr; ?> !important;
}

.sticky .sub-menu li:hover a {
	background-color: <?php echo $nd_clr; ?> !important;
}

/* Sticky Quick Icons */

.sticky #quick-icons li {
	color: <?php echo $nd_clr; ?>;
}

html.no-touch .sticky #quick-icons li:hover {
	border-color: <?php echo $nd_clr; ?>;
}

/* Sticky Nav Icons */

.sticky-icons li{
	color: <?php echo $nd_clr; ?> !important;
}
	
/* Filters */

.port-filter li.selected {
	background-color: <?php echo $fs_clr; ?>;
}

/* Slills */

.team-skills li .bar-val {
	background: <?php echo $fs_clr; ?>;
}

/* Mobile Menu */

#mobile-menu {
	background-color: <?php echo $nd_clr; ?>;
}

#mobile-menu li:hover {
	background: <?php echo $fs_clr; ?>;
}

/* Video */

#volume { position: absolute; left: 0; bottom: 0; background: <?php echo $nd_clr; ?>; z-index:9999; color: #fff; padding: 10px; display: block; opacity: 0.2; font-size: 16px; cursor: pointer; }

#volume:hover { opacity: 1; }

#internal-video:after { content: ''; display: block; width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: <?php echo $fs_alpha; ?>; }

/* Typography */

blockquote { 
	border-color: <?php echo $fs_clr; ?>;
	color: <?php echo $fs_clr; ?>; 
}

h1, h2, h3 { color: <?php echo $fs_clr; ?>; }

.ui-tabs .ui-tabs-nav li.ui-tabs-active {
	background: <?php echo $fs_clr; ?>;
	color: #fff;
}