<?php $dev_mode = {{index.dev_mode}} ?>
<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Maya
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

$menu = $app->getMenu();

$menu = JFactory::getApplication()->getMenu();
$active = $menu->getActive();
$active_params = '';
$header_type = '';
if (isset($active->id) && $active->id >0){
    
    $active_params = $active->params;
    $st_params = new  JRegistry();
    $st_params->loadString($active_params);
    $st_params_arr = $st_params->toArray();
    if (isset($st_params_arr['bt_header_type'])){
        
        $header_type = $active_params->get('bt_header_type',''); 
    }
}
if( empty($header_type)){
    $header_type = $params->get('bt_header_type','clear');
}
if($task == "edit" || $layout == "form" )
{
    $fullWidth = 1;
}
else
{
    $fullWidth = 0;
}

// Add JavaScript Frameworks
//JHtml::_('bootstrap.framework');
//$doc->addScript('templates/' .$this->template. '/js/template.js');

// Add Stylesheets
//$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

// Load optional RTL Bootstrap CSS
//JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();


if ($this->params->get('sitetitle'))
{
    $bt_title = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
    $bt_title = $sitename;
}
if ($this->params->get('bt_logo_white'))
{
    $logo_white = '<img src="'. JUri::root() . $this->params->get('bt_logo_white') .'" alt="'. $bt_title .'" />';
}

if ($this->params->get('bt_logo_dark'))
{
    $logo_dark = '<img src="'. JUri::root() . $this->params->get('bt_logo_dark') .'" alt="'. $bt_title .'" />';
}
if ($this->params->get('bt_logo_mobile'))
{
    $logo_mobile = '<img class="brd-nd-clr" src="'. JUri::root() . $this->params->get('bt_logo_mobile') .'" alt="'. $bt_title .'" />';
}

$url = $this->baseurl.'/templates/' .$this->template.'/';

// show loading
$bt_loader = $params->get('bt_loading',1);

// preset color

$fs_clr = $params->get('bt_group1','#2574a9');
$nd_clr = $params->get('bt_group2','#22313F');
?>
<?php
  $app = JFactory::getApplication();
  $menu = $app->getMenu()->getActive();
  $pageclass = '';

  if (is_object($menu))
    $pageclass = $menu->params->get('pageclass_sfx');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <script>
        var _rollbarConfig = {
            accessToken: "15a80bbf031144ba8cae8d833df7b81c",
            captureUncaught: true,
            payload: {
                environment: "production"
            }
        };
        !function(r){function o(n){if(e[n])return e[n].exports;var t=e[n]={exports:{},id:n,loaded:!1};return r[n].call(t.exports,t,t.exports,o),t.loaded=!0,t.exports}var e={};return o.m=r,o.c=e,o.p="",o(0)}([function(r,o,e){"use strict";var n=e(1),t=e(4);_rollbarConfig=_rollbarConfig||{},_rollbarConfig.rollbarJsUrl=_rollbarConfig.rollbarJsUrl||"https://cdnjs.cloudflare.com/ajax/libs/rollbar.js/2.3.1/rollbar.min.js",_rollbarConfig.async=void 0===_rollbarConfig.async||_rollbarConfig.async;var a=n.setupShim(window,_rollbarConfig),l=t(_rollbarConfig);window.rollbar=n.Rollbar,a.loadFull(window,document,!_rollbarConfig.async,_rollbarConfig,l)},function(r,o,e){"use strict";function n(r){return function(){try{return r.apply(this,arguments)}catch(r){try{console.error("[Rollbar]: Internal error",r)}catch(r){}}}}function t(r,o){this.options=r,this._rollbarOldOnError=null;var e=s++;this.shimId=function(){return e},window&&window._rollbarShims&&(window._rollbarShims[e]={handler:o,messages:[]})}function a(r,o){var e=o.globalAlias||"Rollbar";if("object"==typeof r[e])return r[e];r._rollbarShims={},r._rollbarWrappedError=null;var t=new p(o);return n(function(){o.captureUncaught&&(t._rollbarOldOnError=r.onerror,i.captureUncaughtExceptions(r,t,!0),i.wrapGlobals(r,t,!0)),o.captureUnhandledRejections&&i.captureUnhandledRejections(r,t,!0);var n=o.autoInstrument;return(void 0===n||n===!0||"object"==typeof n&&n.network)&&r.addEventListener&&(r.addEventListener("load",t.captureLoad.bind(t)),r.addEventListener("DOMContentLoaded",t.captureDomContentLoaded.bind(t))),r[e]=t,t})()}function l(r){return n(function(){var o=this,e=Array.prototype.slice.call(arguments,0),n={shim:o,method:r,args:e,ts:new Date};window._rollbarShims[this.shimId()].messages.push(n)})}var i=e(2),s=0,d=e(3),c=function(r,o){return new t(r,o)},p=d.bind(null,c);t.prototype.loadFull=function(r,o,e,t,a){var l=function(){var o;if(void 0===r._rollbarDidLoad){o=new Error("rollbar.js did not load");for(var e,n,t,l,i=0;e=r._rollbarShims[i++];)for(e=e.messages||[];n=e.shift();)for(t=n.args||[],i=0;i<t.length;++i)if(l=t[i],"function"==typeof l){l(o);break}}"function"==typeof a&&a(o)},i=!1,s=o.createElement("script"),d=o.getElementsByTagName("script")[0],c=d.parentNode;s.crossOrigin="",s.src=t.rollbarJsUrl,e||(s.async=!0),s.onload=s.onreadystatechange=n(function(){if(!(i||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState)){s.onload=s.onreadystatechange=null;try{c.removeChild(s)}catch(r){}i=!0,l()}}),c.insertBefore(s,d)},t.prototype.wrap=function(r,o,e){try{var n;if(n="function"==typeof o?o:function(){return o||{}},"function"!=typeof r)return r;if(r._isWrap)return r;if(!r._rollbar_wrapped&&(r._rollbar_wrapped=function(){e&&"function"==typeof e&&e.apply(this,arguments);try{return r.apply(this,arguments)}catch(e){var o=e;throw"string"==typeof o&&(o=new String(o)),o._rollbarContext=n()||{},o._rollbarContext._wrappedSource=r.toString(),window._rollbarWrappedError=o,o}},r._rollbar_wrapped._isWrap=!0,r.hasOwnProperty))for(var t in r)r.hasOwnProperty(t)&&(r._rollbar_wrapped[t]=r[t]);return r._rollbar_wrapped}catch(o){return r}};for(var u="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,captureDomContentLoaded,captureLoad".split(","),f=0;f<u.length;++f)t.prototype[u[f]]=l(u[f]);r.exports={setupShim:a,Rollbar:p}},function(r,o){"use strict";function e(r,o,e){if(r){var t;"function"==typeof o._rollbarOldOnError?t=o._rollbarOldOnError:r.onerror&&!r.onerror.belongsToShim&&(t=r.onerror,o._rollbarOldOnError=t);var a=function(){var e=Array.prototype.slice.call(arguments,0);n(r,o,t,e)};a.belongsToShim=e,r.onerror=a}}function n(r,o,e,n){r._rollbarWrappedError&&(n[4]||(n[4]=r._rollbarWrappedError),n[5]||(n[5]=r._rollbarWrappedError._rollbarContext),r._rollbarWrappedError=null),o.handleUncaughtException.apply(o,n),e&&e.apply(r,n)}function t(r,o,e){if(r){"function"==typeof r._rollbarURH&&r._rollbarURH.belongsToShim&&r.removeEventListener("unhandledrejection",r._rollbarURH);var n=function(r){var e=r.reason,n=r.promise,t=r.detail;!e&&t&&(e=t.reason,n=t.promise),o&&o.handleUnhandledRejection&&o.handleUnhandledRejection(e,n)};n.belongsToShim=e,r._rollbarURH=n,r.addEventListener("unhandledrejection",n)}}function a(r,o,e){if(r){var n,t,a="EventTarget,Window,Node,ApplicationCache,AudioTrackList,ChannelMergerNode,CryptoOperation,EventSource,FileReader,HTMLUnknownElement,IDBDatabase,IDBRequest,IDBTransaction,KeyOperation,MediaController,MessagePort,ModalWindow,Notification,SVGElementInstance,Screen,TextTrack,TextTrackCue,TextTrackList,WebSocket,WebSocketWorker,Worker,XMLHttpRequest,XMLHttpRequestEventTarget,XMLHttpRequestUpload".split(",");for(n=0;n<a.length;++n)t=a[n],r[t]&&r[t].prototype&&l(o,r[t].prototype,e)}}function l(r,o,e){if(o.hasOwnProperty&&o.hasOwnProperty("addEventListener")){for(var n=o.addEventListener;n._rollbarOldAdd&&n.belongsToShim;)n=n._rollbarOldAdd;var t=function(o,e,t){n.call(this,o,r.wrap(e),t)};t._rollbarOldAdd=n,t.belongsToShim=e,o.addEventListener=t;for(var a=o.removeEventListener;a._rollbarOldRemove&&a.belongsToShim;)a=a._rollbarOldRemove;var l=function(r,o,e){a.call(this,r,o&&o._rollbar_wrapped||o,e)};l._rollbarOldRemove=a,l.belongsToShim=e,o.removeEventListener=l}}r.exports={captureUncaughtExceptions:e,captureUnhandledRejections:t,wrapGlobals:a}},function(r,o){"use strict";function e(r,o){this.impl=r(o,this),this.options=o,n(e.prototype)}function n(r){for(var o=function(r){return function(){var o=Array.prototype.slice.call(arguments,0);if(this.impl[r])return this.impl[r].apply(this.impl,o)}},e="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,_createItem,wrap,loadFull,shimId,captureDomContentLoaded,captureLoad".split(","),n=0;n<e.length;n++)r[e[n]]=o(e[n])}e.prototype._swapAndProcessMessages=function(r,o){this.impl=r(this.options);for(var e,n,t;e=o.shift();)n=e.method,t=e.args,this[n]&&"function"==typeof this[n]&&("captureDomContentLoaded"===n||"captureLoad"===n?this[n].apply(this,[t[0],e.ts]):this[n].apply(this,t));return this},r.exports=e},function(r,o){"use strict";r.exports=function(r){return function(o){if(!o&&!window._rollbarInitialized){r=r||{};for(var e,n,t=r.globalAlias||"Rollbar",a=window.rollbar,l=function(r){return new a(r)},i=0;e=window._rollbarShims[i++];)n||(n=e.handler),e.handler._swapAndProcessMessages(l,e.messages);window[t]=n,window._rollbarInitialized=!0}}}}]);
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TFXH8XG');</script>
<!-- End Google Tag Manager -->
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    
    <!-- META -->
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <jdoc:include type="head" />
    <?php if($dev_mode): ?>
        <?php if ($bt_loader):?>  
            <style type="text/css">
            #jpreOverlay{display: none !important;}
            </style>
        <?php endif;?>
        <!-- CSS LIBS -->
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/styles.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/animate.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/custom.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/btcustom.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/grid.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/icons.css' rel='stylesheet' type="text/css">
        <?php if($bt_loader): ?>
            <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/jpreloader.css' rel='stylesheet' type="text/css">
        <?php endif;?>
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/jquery-ui.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/jquery.fancybox.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/lightbox.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/owl.carousel.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/owl.transitions.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/responsive.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/YTPlayer.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/featherlight.min.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/featherlight.gallery.min.css' rel='stylesheet' type="text/css">
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/paver.min.css' rel='stylesheet' type="text/css">
    <?php else: ?>
        <?php if ($bt_loader):?>  
            <style type="text/css">
            #jpreOverlay{display: none !important;}
            </style>
        <?php endif;?>
        <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/all_styles.min.css' rel='stylesheet' type="text/css">
        <?php if($bt_loader): ?>
            <link href='<?php echo JUri::root().'templates/'.$this->template.'/'; ?>css/jpreloader.css' rel='stylesheet' type="text/css">
        <?php endif;?>
    <?php endif;?>

    <!-- GOOGLE FONTS -->
    {{index.google_font}}
    <!-- LESS stylesheet for managing color presets -->
    <!-- <link rel="stylesheet/less" type="text/css" 
        href="<?php echo JURI::base(true); ?>/templates/<?php echo $this->template.'/'; ?>less/color.php?group1=<?php echo  str_replace('#','',$fs_clr); ?>&amp;group2=<?php echo  str_replace('#','',$nd_clr); ?>"> -->
    <!-- LESS JS engine-->
    <!-- <script src="<?php echo JURI::base(true); ?>/templates/<?php echo $this->template.'/'; ?>less/less-1.5.0.min.js"></script> -->
    <style type="text/css" id="less:templates-maya-less-color:php-group1-e0861a-group2-ffc222">/* CUSTOM COLORS */
        .fs-clr {
        color: #e0861a!important;
        }
        .fs-clr-hov:hover {
        color: #e0861a;
        }
        .bg-fs-clr {
        background-color: #e0861a;
        }
        .bg-fs-clr-hov:hover {
        background-color: #e0861a;
        }
        .bg-fs-alpha {
        background-color: rgba(224, 134, 26, 0.7);
        }
        .brd-fs-clr {
        border-color: #e0861a;
        }
        .brd-fs-clr-hov:hover {
        border-color: #e0861a;
        }
        .nd-clr {
        color: #ffc222;
        }
        .nd-clr-hov:hover {
        color: #ffc222;
        }
        .bg-nd-clr {
        background-color: #ffc222;
        }
        .bg-nd-clr-hov:hover {
        background-color: #ffc222;
        }
        .bg-nd-alpha {
        background-color: rgba(255, 194, 34, 0.7);
        }
        .brd-nd-clr {
        border-color: #ffc222;
        }
        .brd-nd-clr-hov:hover {
        border-color: #e0861a;
        }
        /* Preloader */
        #jpreBar {
        background: #e0861a;
        }
        #jprePercentage {
        border-color: #e0861a;
        color: #e0861a;
        }
        /* Standard Menu/Submenu */
        .main-menu li:hover a {
        color: #ffc222;
        }
        .header-dark .sub-menu li a {
        background-color: #ffc222 !important;
        }
        .header-dark .sub-menu li a:hover {
        background-color: #fff !important;
        color: #ffc222;
        }
        .sub-menu li:hover a {
        color: #ffc222 !important;
        }
        /* Sticky Menu/Submenu */
        .sticky .main-menu li,
        .sticky .main-menu li a {
        color: #ffc222;
        }
        .sticky .main-menu li:hover a {
        background-color: #ffc222;
        }
        .sticky .sub-menu li a {
        background-color: #e0861a !important;
        }
        .sticky .sub-menu li:hover a {
        background-color: #ffc222 !important;
        }
        /* Sticky Quick Icons */
        .sticky #quick-icons li {
        color: #ffc222;
        }
        html.no-touch .sticky #quick-icons li:hover {
        border-color: #ffc222;
        }
        /* Sticky Nav Icons */
        .sticky-icons li {
        color: #ffc222 !important;
        }
        /* Filters */
        .port-filter li.selected {
        background-color: #e0861a;
        }
        /* Slills */
        .team-skills li .bar-val {
        background: #e0861a;
        }
        /* Mobile Menu */
        #mobile-menu {
        background-color: #ffc222;
        }
        #mobile-menu li:hover {
        background: #e0861a;
        }
        /* Video */
        #volume {
        position: absolute;
        left: 0;
        bottom: 0;
        background: #ffc222;
        z-index: 9999;
        color: #fff;
        padding: 10px;
        display: block;
        opacity: 0.2;
        font-size: 16px;
        cursor: pointer;
        }
        #volume:hover {
        opacity: 1;
        }
        #internal-video:after {
        content: '';
        display: block;
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        top: 0;
        background: rgba(224, 134, 26, 0.7);
        }
        /* Typography */
        blockquote {
        border-color: #e0861a;
        color: #e0861a;
        }
        h1,
        h2,
        h3 {
        color: #e0861a;
        }
        .ui-tabs .ui-tabs-nav li.ui-tabs-active {
        background: #e0861a;
        color: #fff;
        }
        .white-color{
            color: #ffffff!important;
        }
</style>
    <!--[if lt IE 9]>
        <script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
    <![endif]-->
    <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
</head>

<body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
    echo $pageclass ? ' '.htmlspecialchars($pageclass) : 'default';
?>">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TFXH8XG"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <input type="hidden" id="bt_loader" value="<?php echo $bt_loader;?>" />
    <div id="wrap">
    
        <header class="transit header-<?php echo $header_type; ?>">
    
        <div id="logo-white" class="logo"><a href="<?php echo JUri::root();?>">
        <?php if($header_type == 'clear'){
            if($params->get('bt_logo_white') != ''){echo $logo_white;}else{ echo $bt_title;}
            
        }else{
            if($params->get('bt_logo_dark') != ''){echo $logo_dark;}else{ echo $bt_title;}
        }?>
        </a></div>
        <div id="logo-dark" class="logo"><a href="<?php echo JUri::root();?>">
        <?php 
        if($params->get('bt_logo_dark') != ''){echo $logo_dark;}else{ echo $bt_title;}
        ?>
        </a></div>
        <div id="logo-short" class="logo"><a href="<?php echo JUri::root();?>">
        <?php 
        if($params->get('bt_logo_mobile') != ''){echo $logo_mobile;}else{ echo $bt_title;}
        ?>
        </a></div>
    
        <nav class="main-menu">
                
            <!-- main menu 
                ================================================== -->
            <?php if ($this->countModules('menu')): ?>
            <jdoc:include type="modules" name="menu" style="xhtml" />
            <?php endif; ?>
            <!-- end main menu 
                ================================================== -->
            
        </nav>
        
        <div id="quick-icons">
            
            <ul class="info-menu">
                <!-- <li><a title="Facebook" href="{{index.facebook_url}}" target="_blank"><i class="icon-facebook"></i></a></li>
                <li><a title="Twitter" href="{{index.twitter_url}}" target="_blank"><i class="icon-twitter"></i></a></li> -->
                <!-- <li><a title="Instagram" href="{{index.instagram_url}}" target="_blank"><i class="icon-instagram"></i></a></li> -->
                <!-- <li><a title="ISSUU" href="{{index.issuu_url}}" target="_blank"><span class="issuu-icon"></span></a></li> -->
            </ul>
            
            <ul class="switch-menu">
                <li class="open-menu"><i class="icon-menu"></i></li>
            </ul>
                   
            <!-- mobile language 
                ================================================== -->
            <?php if ($this->countModules('language')): ?>
            <jdoc:include type="modules" name="language" style="intro" />
            <?php endif; ?>
            <!-- end mobile language -->
            
            <div class="clear"></div>
            
        </div>
        
        <div class="clear"></div>
        
    </header>
    
    <nav id="mobile-menu">
                
        <!-- mobile menu 
            ================================================== -->
        <?php if ($this->countModules('menu')): ?>
        <jdoc:include type="modules" name="menu" style="intro" />
        <?php endif; ?>
        <!-- end mobile menu 
            ================================================== -->
            
    </nav>
    <!-- // MENU / LOG -->
    
        <!-- home 
            ================================================== -->
        <?php 
            $currentURL = JURI::current();
            $currentURL_array = explode('/', $currentURL);
                // This condition means feature module position won't show in the festivals sub-pages - it should work everywhere else as fesivals isn't used in the URL as [url start]/festivals/[url end]
                if ( ($currentURL_array[count($currentURL_array)-2]  != 'festivals') && ($currentURL_array[count($currentURL_array)-2]  != 'courses') && ($currentURL_array[count($currentURL_array)-2]  != 'meditation-through-music') && ($currentURL_array[count($currentURL_array)-2]  != 'introductory-events') && ($currentURL_array[count($currentURL_array)-2]  != 'beginner') && ($currentURL_array[count($currentURL_array)-2]  != 'intermediate') && ($currentURL_array[count($currentURL_array)-2]  != 'advanced') && ($currentURL_array[count($currentURL_array)-2 ]  != 'all') && ($currentURL_array[count($currentURL_array)-2 ]  != 'regular') && ($currentURL_array[count($currentURL_array)-2 ]  != 'community-news') && ($currentURL_array[count($currentURL_array)-2 ]  != 'press-and-media') && ($currentURL_array[count($currentURL_array)-2 ]  != 'volunteer') && ($currentURL_array[count($currentURL_array)-2 ]  != 'work-for-us') && ($currentURL_array[count($currentURL_array)-2 ]  != '50th-anniversary') && ($currentURL_array[count($currentURL_array)-2 ]  != '50th-anniversary-dev') ){
                    // This is to debug the array
                    // print_r($currentURL_array);
        ?>
                    <?php if ($this->countModules('feature')): ?>
                        <div id="intro" class="section">
                            <jdoc:include type="modules" name="feature" style="none" /> 
                        </div>
                    <?php endif; ?>
                <?php 
                    }
                    elseif ( ($currentURL_array[count($currentURL_array)-1]  == 'beginner') || ($currentURL_array[count($currentURL_array)-1]  == 'intermediate') || ($currentURL_array[count($currentURL_array)-1]  == 'advanced') || ($currentURL_array[count($currentURL_array)-1] == 'all') || ($currentURL_array[count($currentURL_array)-1] == 'regular') && ($currentURL_array[count($currentURL_array)-2]  == 'courses') ){
                ?>
                    <?php if ($this->countModules('feature')): ?>
                        <div id="intro" class="section">
                            <jdoc:include type="modules" name="feature" style="none" /> 
                        </div>
                    <?php endif;
                }
                ?>
        <!-- End home -->
        <!-- user1 
            ================================================== -->
        <?php 
            $currentURL = JURI::current();
            $currentURL_array = explode('/', $currentURL);
                // This condition means feature module position won't show in the festivals sub-pages - it should work everywhere else as fesivals isn't used in the URL as [url start]/festivals/[url end]
                if ( ($currentURL_array[count($currentURL_array)-2]  != 'festivals') && ($currentURL_array[count($currentURL_array)-2]  != 'courses') && ($currentURL_array[count($currentURL_array)-2]  != 'introductory-events') && ($currentURL_array[count($currentURL_array)-2]  != 'beginner') && ($currentURL_array[count($currentURL_array)-2]  != 'intermediate') && ($currentURL_array[count($currentURL_array)-2]  != 'advanced') && ($currentURL_array[count($currentURL_array)-2]  != 'all') && ($currentURL_array[count($currentURL_array)-2]  != 'regular') && ($currentURL_array[count($currentURL_array)-2 ]  != 'community-news') && ($currentURL_array[count($currentURL_array)-2 ]  != 'press-and-media') && ($currentURL_array[count($currentURL_array)-2 ]  != 'volunteer') && ($currentURL_array[count($currentURL_array)-2 ]  != 'work-for-us') ){
        ?>
                    <?php if ($this->countModules('user1')): ?>
                    <jdoc:include type="modules" name="user1" style="xhtml" />
                    <?php endif; ?>
                    <!-- end user1 
                        ================================================== -->
            <?php 
                }
                elseif ( ($currentURL_array[count($currentURL_array)-1]  == 'beginner') || ($currentURL_array[count($currentURL_array)-1]  == 'intermediate') || ($currentURL_array[count($currentURL_array)-1]  == 'advanced') || ($currentURL_array[count($currentURL_array)-1]  == 'all') || ($currentURL_array[count($currentURL_array)-1]  == 'regular') && ($currentURL_array[count($currentURL_array)-2]  == 'courses') ){ 
                ?>
                        <?php if ($this->countModules('user1')): ?>
                        <jdoc:include type="modules" name="user1" style="xhtml" />
                        <?php endif; ?>
            <?php }
            ?>
        <!-- user2 
            ================================================== -->
        <?php if ($this->countModules('user2')): ?>
        <jdoc:include type="modules" name="user2" style="xhtml" />
        <?php endif; ?>
        <!-- end user2 
            ================================================== -->
        <!-- user3 
            ================================================== -->
        <?php if ($this->countModules('user3')): ?>
        <jdoc:include type="modules" name="user3" style="xhtml" />
        <?php endif; ?>
        <!-- end user3 
            ================================================== -->
            <!-- user4 
            ================================================== -->
        <?php if ($this->countModules('user4')): ?>
        <jdoc:include type="modules" name="user4" style="xhtml" />
        <?php endif; ?>
        <!-- end user4 
            ================================================== -->
        <?php if ($option == 'com_k2'){?>
        <?php if ($view == 'itemlist'){?>
        <div id="blog" class="page">
        <?php if ($this->countModules('left') || $this->countModules('right')){ ?>
        <!-- <div id="blog-image" style="background-image: url('<-?php echo JUri::root().'images/background/Sankirtan1.jpg'; ?>');"></div>
        <div class="header-page padd-y-75 bg-gr1-clr">
                    
            <div class="title-page padd-x-25 fs-clr"><?php  echo $this->title;?></div>
            
        </div>
        <div class="post-wrap padd-y-50 boxed "> -->
        <?php }else{ ?> 
        <!-- <div id="blog-image" style="background-image: url('<?php echo JUri::root().'images/background/Sankirtan1.jpg'; ?>');"></div>
        <div class="header-page padd-y-75 bg-gr1-clr">
                    
            <div class="title-page padd-x-25 fs-clr"><-?php  echo $this->title;?></div>
            
        </div> -->
        <?php }?>
        <?php if ($this->countModules('left')): ?>
                                            
                <aside class="col-1-3">
                <!-- left
                
                    ================================================== -->
                <jdoc:include type="modules" name="left" style="well" />
                
                <!-- end left 
                    ================================================== -->
                </aside>
            <?php endif; ?>
            <article class="<?php if ($this->countModules('left') || $this->countModules('right')) {echo 'col-2-3';}elseif($this->countModules('left') && $this->countModules('left')) {echo 'col-1-3';}else{echo '';}?>">
                <!-- Begin Content -->
                <jdoc:include type="message" />
                <jdoc:include type="component" />
                <!-- End Content -->
            </article>
            <?php if ($this->countModules('right')): ?>
                                            
                <aside class="col-1-3">
                <!-- right
                
                    ================================================== -->
                <jdoc:include type="modules" name="right" style="well" />
                
                <!-- end right 
                    ================================================== -->
                </aside>
            <?php endif; ?>
        <?php if ($this->countModules('left') || $this->countModules('right')): ?>
        </div>
        <?php endif;?>
        <div class="clearfix"></div>
        </div>
        <?php }else{ ?> 
        <main id="content" role="main" >
        <div id="item" class="page">
            <?php if ($this->countModules('left') || $this->countModules('right')): ?>
        <div class="post-wrap padd-y-50 boxed" style="margin-top: 150px;">
        <?php endif;?>
        <?php if ($this->countModules('left')): ?>
                                            
                <aside class="col-1-3">
                <!-- left
                
                    ================================================== -->
                <jdoc:include type="modules" name="left" style="well" />
                
                <!-- end left 
                    ================================================== -->
                </aside>
            <?php endif; ?>
            <article class="<?php if ($this->countModules('left') || $this->countModules('right')) {echo 'col-2-3';}elseif($this->countModules('left') && $this->countModules('left')) {echo 'col-1-3';}else{echo '';}?>">
                <!-- Begin Content -->
                <jdoc:include type="message" />
                <jdoc:include type="component" />
                <!-- End Content -->
            </article>
            <?php if ($this->countModules('right')): ?>
                                            
                <aside class="col-1-3">
                <!-- right
                
                    ================================================== -->
                <jdoc:include type="modules" name="right" style="well" />
                
                <!-- end right 
                    ================================================== -->
                </aside>
            <?php endif; ?>
        <?php if ($this->countModules('left') || $this->countModules('right')): ?>
        </div>
        <?php endif;?>
        
        <div class="clearfix"></div>
        </div>
        </main>
        <?php }?>
       <?php  }else{ ?> 
        <main id="content" role="main" >
        
        <div class="post-wrap padd-y-50 boxed">
            <?php if ($this->countModules('left')): ?>
                                            
                <aside class="col-1-3">
                <!-- left
                
                    ================================================== -->
                <jdoc:include type="modules" name="left" style="well" />
                
                <!-- end left 
                    ================================================== -->
                </aside>
            <?php endif; ?>
            <article class="<?php if ($this->countModules('left') || $this->countModules('right')) {echo 'col-2-3';}elseif($this->countModules('left') && $this->countModules('left')) {echo 'col-1-3';}else{echo '';}?>">
                <!-- Begin Content -->
                <jdoc:include type="message" />
                <jdoc:include type="component" />
                <!-- End Content -->
            </article>
            <?php if ($this->countModules('right')): ?>
                                            
                <aside class="col-1-3">
                <!-- right
                
                    ================================================== -->
                <jdoc:include type="modules" name="right" style="well" />
                
                <!-- end right 
                    ================================================== -->
                </aside>
            <?php endif; ?>
        </div>
        </main>
        <?php } ?>
            <!-- user5
            ================================================== -->
        <?php if ($this->countModules('user5')): ?>
        <jdoc:include type="modules" name="user5" style="xhtml" />
        <?php endif; ?>
        <!-- end user5 
            ================================================== -->
            <!-- user6
            ================================================== -->
        <?php if ($this->countModules('user6')): ?>
        <jdoc:include type="modules" name="user6" style="xhtml" />
        <?php endif; ?>
        <!-- end user6 
            ================================================== -->
       
        <div id="widgets" class="section">

    <div class="widget-content padd-y-50 bg-fs-clr">
    
        <div class="boxed">
    
            <div class="col-1-3">
            
                <jdoc:include type="modules" name="footer1" style="footer" />
                
            </div>
        
            <div class="col-1-3">
            
                <jdoc:include type="modules" name="footer2" style="footer" />
            </div>

            <div class="col-1-3">
            
                <jdoc:include type="modules" name="footer3" style="footer" />
            
            </div>
        
            <div class="clear"></div>
        
        </div>

    </div>
    
</div><!-- // WIDGETS -->
        <!-- copyright 
            ================================================== -->
        <footer>
            <div class="container">
                <?php if ($this->countModules('copyright')): ?>
                <jdoc:include type="modules" name="copyright" style="xhtml" />
                <?php endif; ?>
            </div>
        </footer>
        <!-- End copyright -->
    </div>
    <!-- End Container -->
    
    <jdoc:include type="modules" name="debug" style="none" />
    

    <div class="btn-up brd-wh-clr bg-wh-clr fs-clr brd-fs-clr-hov">        
         <i class="icon-arrow-up"></i>
    </div>

    <div id="curtain"></div>
    
    <!-- JS LIBS -->
    <?php if($dev_mode): ?>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/jquery-min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/modernizr-custom.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/featherlight.min.js"></script> 
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/featherlight.gallery.min.js"></script> 
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/swipe.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/easing-min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/gmap.api.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/gmap.framework.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jpreloader.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.countTo.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.fancybox.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.fittext.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.mb.YTPlayer.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/jquery.ui.map.full.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/modernizer-min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/full-versions/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/jflickrfeed.min.js"></script> 
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/fa-v4-shims.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/fontawesome-all.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/scripts.js"></script> 
        <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/custom_scripts.js"></script>
    <?php else: ?>
            <script type="text/javascript" src="<?php echo JUri::root().'templates/'.$this->template.'/'; ?>js/all_scripts.min.js"></script>
    <?php endif;?>
</body>
</html>
