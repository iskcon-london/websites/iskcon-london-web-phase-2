<?php
/*
# ------------------------------------------------------------------------
# Vina Item Carousel for K2 for Joomla 3
# ------------------------------------------------------------------------
# Copyright(C) 2014 www.VinaGecko.com. All Rights Reserved.
# @license http://www.gnu.org/licenseses/gpl-3.0.html GNU/GPL
# Author: VinaGecko.com
# Websites: http://vinagecko.com
# Forum: http://vinagecko.com/forum/
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Load CSS files
if(!defined('VINA_CAROUSEL_DEFAULT')) {
	$doc->addStyleSheet(JURI::base() . 'templates/maya/html/mod_vina_carousel_k2/' . $template . '/css/default.css');
	define('VINA_CAROUSEL_DEFAULT', 1);
}
?>
<div id="vina-carousel-k2-wrapper<?php echo $module->id; ?>" class="<?php echo $classSuffix; ?>">
	<div id="vina-carousel-k2<?php echo $module->id; ?>" class="vina-carousel-default owl-carousel owl-theme">
		<?php
			$col = $nb = 1;

			foreach($lists as $key => $item) :
				$nb ++;
				
				$image = $item->$itemImgSize;
				$title = $item->title;
				$link  = $item->link;
				$cname = $item->categoryname;
				$clink = $item->categoryLink;
				$hits  = $item->hits;
				$introtext = $item->introtext;
				$created   = $item->created;
			?>
			<?php if($col == 1) : ?>
			<div class="items">
				<?php endif; ?>
		
				<!-- Start Item Content -->
				<div class="item-<?php echo $col; ?>">
					<!-- Image Block -->
					<?php if($imagePosition == 1 &&$itemImage && !empty($image)) : ?>
					<div class="image-block">
						<a href="<?php echo $link; ?>" title="<?php echo $title; ?>">
							<img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
						</a>
					</div>
					<?php endif; ?>
			
					<!-- Text Block -->
					<?php if($itemTitle || $itemIntroText || $itemCategory || $itemDateCreated || $itemHits || $itemReadMore) : ?>
					<div class="text-block">
						<!-- Title Block -->
						<?php if($itemTitle) :?>
						<h3 class="title">
							<a href="<?php echo $link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
						</h3>
						<?php endif; ?>
						
						<!-- Info Block -->
						<?php if($itemCategory || $itemDateCreated || $itemHits) : ?>
						<div class="info">
							<?php if($itemDateCreated) : ?>
							<span><?php echo JTEXT::_('VINA_PUBLISHED'); ?>: <?php echo JHTML::_('date', $created, 'F d, Y');?></span>
							<?php endif; ?>
							
							<?php if($itemCategory) : ?>
							<span><?php echo JTEXT::_('VINA_CATEGORY'); ?>: <a href="<?php echo $clink; ?>"><?php echo $cname; ?></a></span>
							<?php endif; ?>
							
							<?php if($itemHits) : ?>
							<span><?php echo JTEXT::_('VINA_HITS'); ?>: <?php echo $hits; ?></span>
							<?php endif; ?>
						</div>
						<?php endif; ?>
						
						<!-- Intro text Block -->
						<?php if($itemIntroText) : ?>
						<div class="introtext"><?php echo $introtext; ?></div>
						<?php endif; ?>
						
						<!-- Extra Fields Block -->
						<?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
						<div class="moduleItemExtraFields">
							<b><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></b>
							<ul>
							<?php foreach ($item->extra_fields as $extraField): ?>
									<?php if($extraField->value != ''): ?>
									<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
										<?php if($extraField->type == 'header'): ?>
										<h4 class="moduleItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
										<?php else: ?>
										<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
										<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
										<?php endif; ?>
										<div class="clr"></div>
									</li>
									<?php endif; ?>
							<?php endforeach; ?>
							</ul>
						</div>
						<?php endif; ?>
				
						<!-- Readmore Block -->
						<?php if($itemReadMore) : ?>
						<div class="readmore">
							<a class="buttonlight morebutton" href="<?php echo $link; ?>" title="<?php echo $title; ?>">
								<?php echo JText::_('VINA_READ_MORE'); ?>
							</a>
						</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>
					
					<!-- Image Block -->
					<?php if($imagePosition == 0 &&$itemImage && !empty($image)) : ?>
					<div class="image-block">
						<a href="<?php echo $link; ?>" title="<?php echo $title; ?>">
							<img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
						</a>
					</div>
					<?php endif; ?>
				</div>
			<?php 
				if($col == $itemInCol || $nb > count($lists)) {
					$col = 1;
					echo '</div>';
				}
				else {
					$col ++;
				}
			endforeach;
		// End Foreach
		?>
	</div>
</div>