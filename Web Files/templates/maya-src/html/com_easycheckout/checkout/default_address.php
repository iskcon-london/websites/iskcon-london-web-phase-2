<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */

// No direct access to this file
defined( '_JEXEC' ) or die;
?>

<?php if($this->user->id > 0): ?>
	<!-- User is logged in -->

	<?php if (isset($this->addresses) && count($this->addresses) > 0) : ?>
		<!-- Customer already has existing addresses List them out -->
		<?php echo $this->loadTemplate('list_address'); ?>
	<?php else: ?>
		<?php echo $this->loadTemplate('address_form_new'); ?>
	<?php endif; ?>

<?php else: ?>
    <?php if($this->storeProfile->get( 'allow_registration', 1 ) == 1 || $this->storeProfile->get( 'allow_guest_checkout', 0 ) == 1 ):?>
        <?php echo $this->loadTemplate('address_form_new'); ?>
    <?php endif; ?>
<?php endif; ?>