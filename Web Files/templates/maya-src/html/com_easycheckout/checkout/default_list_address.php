<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */

// No direct access to this file
defined( '_JEXEC' ) or die;
?>
<div id="billing-blog">
    <h3><?php echo JText::_('J2STORE_BILLING_ADDRESS');?></h3>
    <input type="radio" name="billing_address_type" value="existing" id="billing-address-existing" checked="checked" />
    <label for="billing-address-existing"><?php echo JText::_('J2STORE_ADDRESS_EXISTING'); ?></label>
    <div id="billing-existing">
        <select id="existing_address_id" name="address_id" style="width: 100%; margin-bottom: 15px;" size="5">

            <?php foreach ($this->addresses as $address) : ?>
                <?php if(empty($this->address_id)){
                    $this->address_id = $address->j2store_address_id;
                } ?>
                <?php if ($address->j2store_address_id == $this->address_id) : ?>
                    <option value="<?php echo $address->j2store_address_id; ?>" selected="selected">
                        <?php echo $address->first_name; ?> <?php echo $address->last_name; ?>, <?php echo $address->address_1; ?>, <?php echo $address->city; ?>, <?php echo $address->zip; ?>, <?php echo JText::_($address->zone_name); ?>, <?php echo JText::_($address->country_name); ?>
                    </option>

                <?php else: ?>
                    <option value="<?php echo $address->j2store_address_id; ?>">
                        <?php echo $address->first_name; ?> <?php echo $address->last_name; ?>, <?php echo $address->address_1; ?>, <?php echo $address->city; ?>, <?php echo $address->zip; ?>, <?php echo JText::_($address->zone_name); ?>, <?php echo JText::_($address->country_name); ?>
                    </option>
                <?php endif; ?>

            <?php endforeach; ?>
        </select>
    </div>
    <style>
        #billing_add_address_model .modal-body{
            overflow-y: auto;
        }
    </style>
    <?php
    $version_file = JPATH_ADMINISTRATOR . '/components/com_j2store/version.php';
    if (JFile::exists($version_file)) {
        require_once($version_file);
    }

    if (version_compare(J2STORE_VERSION, '3.3.6', 'lt')){
?>
        <a data-toggle="modal" class="btn btn-success" href="<?php echo JRoute::_('index.php?option=com_easycheckout&view=checkout&task=get_checkout_form&address_type=billing&tmpl=component');?>" data-target="#billing_add_address_model"><?php echo JText::_('EASYCHECKOUT_ADD_BILLING_ADDRESS');?></a>

        <!-- Modal -->
        <div class="modal fade" id="billing_add_address_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="billing_add_address_model_btn" onclick="saveAddress('billing_add_address_model','billing')"><?php echo JText::_('J2STORE_SAVE');?></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo JText::_('J2STORE_CLOSE');?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <?php
    }else{
        ?>
        <a data-fancybox data-type="ajax" class="btn btn-success" data-src="<?php echo JRoute::_('index.php?option=com_easycheckout&view=checkout&task=get_checkout_form&address_type=billing&tmpl=component');?>" href="javascript:;">
            <?php echo JText::_('EASYCHECKOUT_ADD_BILLING_ADDRESS');?>
        </a>
        <?php
    }
    ?>
    <?php echo J2Store::plugin()->eventWithHtml('CheckoutBilling', array($this));?>
</div>
<br>
<?php
if($this->showShipping):?>
<div id="shipping-blog">
    <h3><?php echo JText::_('J2STORE_SHIPPING_ADDRESS');?></h3>
    <input type="radio" name="shipping_address_type" value="existing" id="shipping-address-existing" checked="checked" />
    <label for="shipping-address-existing"><?php echo JText::_('J2STORE_ADDRESS_EXISTING'); ?></label>
    <div id="shipping-existing">
        <select id="existing_shipping_address_id" name="shipping_address_id" style="width: 100%; margin-bottom: 15px;" size="5">

            <?php foreach ($this->addresses as $address) : ?>
                <?php if(empty($this->shipping_address_id)){
                    $this->shipping_address_id = $address->j2store_address_id;
                } ?>
                <?php if ($address->j2store_address_id == $this->shipping_address_id) : ?>
                    <option value="<?php echo $address->j2store_address_id; ?>" selected="selected">
                        <?php echo $address->first_name; ?> <?php echo $address->last_name; ?>, <?php echo $address->address_1; ?>, <?php echo $address->city; ?>, <?php echo $address->zip; ?>, <?php echo JText::_($address->zone_name); ?>, <?php echo JText::_($address->country_name); ?>
                    </option>

                <?php else: ?>
                    <option value="<?php echo $address->j2store_address_id; ?>">
                        <?php echo $address->first_name; ?> <?php echo $address->last_name; ?>, <?php echo $address->address_1; ?>, <?php echo $address->city; ?>, <?php echo $address->zip; ?>, <?php echo JText::_($address->zone_name); ?>, <?php echo JText::_($address->country_name); ?>
                    </option>
                <?php endif; ?>

            <?php endforeach; ?>
        </select>
    </div>
    <?php if (version_compare(J2STORE_VERSION, '3.3.6', 'lt')): ?>
        <style>
            #shipping_add_address_model .modal-body{
                overflow-y: auto;
            }
        </style>
        <a data-toggle="modal" class="btn btn-success" href="<?php echo JRoute::_('index.php?option=com_easycheckout&view=checkout&task=get_checkout_form&address_type=shipping&tmpl=component');?>" data-target="#shipping_add_address_model"><?php echo JText::_('EASYCHECKOUT_ADD_SHIPPING_ADDRESS');?></a>

        <!-- Modal -->
        <div class="modal fade" id="shipping_add_address_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="shipping_add_address_model_btn" onclick="saveAddress('shipping_add_address_model','shipping')"><?php echo JText::_('J2STORE_SAVE');?></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo JText::_('J2STORE_CLOSE');?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    <?php else: ?>
        <a data-fancybox data-type="ajax" class="btn btn-success" data-src="<?php echo JRoute::_('index.php?option=com_easycheckout&view=checkout&task=get_checkout_form&address_type=shipping&tmpl=component');?>" href="javascript:;">
            <?php echo JText::_('EASYCHECKOUT_ADD_SHIPPING_ADDRESS');?>
        </a>
    <?php endif; ?>
    <?php echo J2Store::plugin()->eventWithHtml('CheckoutShipping', array($this));?>
</div>
<?php endif; ?>