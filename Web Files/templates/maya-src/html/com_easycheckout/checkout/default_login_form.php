<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */

// No direct access to this file
defined( '_JEXEC' ) or die;

$forgot_pass_link = JRoute::_('index.php?option=com_users&view=reset');
?>
<div class="easy-checkout-returning-customer easy-checkout-column" id="easy-checkout-returning-customer">

	<h4 class="easy-checkout-returning-customer-heading"><?php echo JText::_('EASYCHECKOUT_RETURNING_CUSTOMER'); ?> <a href="javascript:void ();" id="toggle_show_login_form"><?php echo JText::_('J2STORE_CHECKOUT_LOGIN'); ?></a></h4>

	<div id="easy-returning-customer-login-form" style="display: none;">

		<div class="<?php echo $this->row_class; ?>">
			<div class="<?php echo $this->col_class;?>3">
				<input type="text" name="email" value="" placeholder="<?php echo JText::_('EASYCHECKOUT_PLACEHOLDER_USERNAME'); ?>"/>
			</div>
			<div class="<?php echo $this->col_class;?>3">
				<input type="password" name="password"  value="" placeholder="<?php echo JText::_('EASYCHECKOUT_PLACEHOLDER_PASSWORD'); ?>" />
			</div>
			<div class="<?php echo $this->col_class;?>6">
				<input type="button" value="<?php echo JText::_('J2STORE_CHECKOUT_LOGIN'); ?>" id="easy-checkout-button-login" class="button btn btn-primary" />
				<a href="<?php echo $forgot_pass_link;?>" target="_blank"><?php echo JText::_('J2STORE_FORGOT_YOUR_PASSWORD'); ?></a>
			</div>
		</div>

		<input type="hidden" name="task" value="login_validate" />
		<input type="hidden" name="option" value="com_easycheckout" />
		<input type="hidden" name="view" value="checkout" />
		<br />
		<?php echo J2Store::plugin()->eventWithHtml('CheckoutLogin', array($this)); ?>

	</div>
</div>
