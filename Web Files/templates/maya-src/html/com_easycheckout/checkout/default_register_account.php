<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */

// No direct access to this file
defined( '_JEXEC' ) or die;
?>
<?php if ( $this->storeProfile->get( 'allow_registration', 1 ) == 1 || $this->storeProfile->get( 'allow_guest_checkout', 0 ) == 0 ): ?>

	<div class="create-account">
        <?php
            $checked = '';
            $show_checkbox = true;
            $show_password = true;
            if($this->storeProfile->get( 'allow_registration', 1 ) == 1 && $this->storeProfile->get( 'allow_guest_checkout', 0 ) == 0 ){
                $checked = 'checked="checked"';
                $show_checkbox = false;
            }elseif(($this->storeProfile->get( 'allow_registration', 1 ) == 1 && $this->storeProfile->get( 'allow_guest_checkout', 0 ) == 1) || ($this->storeProfile->get( 'allow_registration', 1 ) == 0 && $this->storeProfile->get( 'allow_guest_checkout', 0 ) == 0) ){
                $checked = 'checked="checked"';
                $show_checkbox = true;
            }else{
                $show_password = false;
            }

        ?>
		<div class="create-account-checkbox <?php echo $this->row_class; ?> <?php echo $show_checkbox ? '': 'hide';?>" >
			<div class="<?php echo $this->col_class; ?>12">
				<label for="create_account" class="create_account_label">
					<input type="checkbox" name="create_account" value="1" <?php echo $checked ? $checked: '';?>/>
					<?php echo JText::_( 'EASYCHECKOUT_CREATE_ACCOUNT_FOR_LATER_USE' ); ?>
				</label>
			</div>
		</div>


		<div id="create-account-fields" class="<?php echo $this->row_class; ?> <?php echo $show_password ? '': 'hide';?>">

			<div class="<?php echo $this->col_class ?>6">
                <input type="text" id="j2_user" name="j2_user" value="" style="visibility: hidden"/>
				<span class="j2store_field_required">*</span>
				<label for="password"><?php echo JText::_( 'EASYCHECKOUT_PASSWORD' ); ?></label>
				<input type="password" id="password" name="password" value=""/>
			</div>
			<div class="<?php echo $this->col_class ?>6">
				<span class="j2store_field_required">*</span>
				<label for="confirm_password"><?php echo JText::_( 'EASYCHECKOUT_CONFIRM_PASSWORD' ); ?></label>
				<input type="password" id="confirm" name="confirm" value=""/>
			</div>
            <div class="<?php echo $this->col_class ?>12">
            <?php if($show_password):?>
                <?php echo J2Store::plugin()->eventWithHtml('CheckoutRegister', array($this)); ?>
            <?php endif; ?>
             </div>
		</div>
	</div>

<?php endif; ?>


