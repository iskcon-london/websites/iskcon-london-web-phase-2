<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
// No direct access to this file
defined( '_JEXEC' ) or die;
$html = $this->storeProfile->get('store_'.$this->address_type.'_layout');
if(empty($html) || JString::strlen($html) < 5) {
    //we dont have a profile set in the store profile. So use the default one.
    $html = $this->loadTemplate('format');
}

//first find all the checkout fields
preg_match_all("^\[(.*?)\]^",$html,$checkoutFields, PREG_PATTERN_ORDER);

$allFields = $this->fields;

?>
<?php foreach ($this->fields as $fieldName => $oneExtraField): ?>
    <?php
    $onWhat='onchange'; if($oneExtraField->field_type=='radio') $onWhat='onclick';
    if(property_exists($this->address, $fieldName) && $fieldName != 'email') {
        $placeholder =  (isset($oneExtraField->field_options['placeholder']) ? $oneExtraField->field_options['placeholder'] : "");
        $field_options = '';
        if($placeholder){
            $field_options .= ' placeholder="'.$placeholder.'" ';
        }
        if($this->address_type == 'billing'){
            $this->fieldsClass->prefix = '';
            $html = str_replace('['.$fieldName.']',$this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, $fieldName,false, $field_options, $test = false, $allFields, $allValues = null).'</br />',$html);
        }else{
            $this->fieldsClass->prefix = 'shipping_';
            $html = str_replace('['.$fieldName.']',$this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, 'shipping_'.$fieldName,false, $field_options, $test = false, $allFields, $allValues = null).'</br />',$html);
        }

    }
    ?>
<?php endforeach; ?>

<?php
//check for unprocessed fields. If the user forgot to add the fields to the checkout layout in store profile, we probably have some.
$unprocessedFields = array();
foreach($this->fields as $fieldName => $oneExtraField) {
    if(!in_array($fieldName, $checkoutFields[1]) &&  $fieldName != 'email') {
        $unprocessedFields[$fieldName] = $oneExtraField;
    }
}

//now we have unprocessed fields. remove any other square brackets found.
preg_match_all("^\[(.*?)\]^",$html,$removeFields, PREG_PATTERN_ORDER);
foreach($removeFields[1] as $fieldName) {
    $html = str_replace('['.$fieldName.']', '', $html);
}

?>
<div id="easy-checkout-new-address">
<?php echo $html; ?>

<?php if(count($unprocessedFields)): ?>
    <div class="<?php echo $this->row_class;?>">
        <div class="<?php echo $this->col_class;?>12">
            <?php $uhtml = '';?>
            <?php foreach ($unprocessedFields as $fieldName => $oneExtraField): ?>
                <?php
                $onWhat='onchange'; if($oneExtraField->field_type=='radio') $onWhat='onclick';
                //echo $this->fieldsClass->display($oneExtraField,@$this->address->$fieldName,$fieldName,false);
                if(property_exists($this->address, $fieldName)) {
                    $placeholder =  (isset($oneExtraField->field_options['placeholder']) ? $oneExtraField->field_options['placeholder'] : "");
                    $field_options = '';
                    if($placeholder){
                        $field_options .= ' placeholder="'.$placeholder.'" ';
                    }

                    if($this->address_type == 'billing'){
                        $this->fieldsClass->prefix = '';
                        $uhtml .= $this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, $fieldName,false, $field_options, $test = false, $allFields, $allValues = null);
                    }else{
                        $this->fieldsClass->prefix = 'shipping_';
                        $uhtml .= $this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, 'shipping_'.$fieldName,false, $field_options, $test = false, $allFields, $allValues = null);
                    }
                    $uhtml .='<br />';
                }
                ?>
            <?php endforeach; ?>
            <?php echo $uhtml; ?>
        </div>
    </div>
<?php endif; ?>
<?php
if($this->address_type == 'billing'){
    ?>
    <input type="hidden" name="email" value="<?php echo JFactory::getUser()->email; ?>" />
    <?php
}else{
    ?>
    <input type="hidden" name="shipping_email" value="<?php echo JFactory::getUser()->email; ?>" />
    <?php
}
?>
    <?php if(isset($this->fancybox) && !empty($this->fancybox)): ?>
        <div class="footer">
            <input type="button" class="btn btn-primary" onclick="saveAddress('easy-checkout-new-address','<?php echo $this->address_type; ?>')" value="<?php echo JText::_('J2STORE_SAVE');?>">
        </div>
    <?php endif; ?>
</div>