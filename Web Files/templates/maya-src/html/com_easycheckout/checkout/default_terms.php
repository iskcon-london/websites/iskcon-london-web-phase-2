<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="j2store-modal">
<div class="modal">
	<div class="j2store">
		<div class="modal-header">
		<a class="close" data-dismiss="modal">&times;</a>
		</div>
		<div class="modal-body">
		<?php echo $this->html; ?>
		</div>
		<div class="modal-footer">
		<a class="btn" data-dismiss="modal"><?php echo JText::_('J2STORE_CLOSE')?></a>
		</div>
	</div>
</div>
</div>