<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php if(isset($this->order)): ?>
		<div class="j2storeOrderSummary">
			<?php echo $this->loadAnyTemplate('site:com_easycheckout/checkout/default_cartsummary'); ?>
		</div>
	<?php endif; ?>
<?php if(isset($this->ec_html)): ?>
			<!--    Express Payment   -->		
	<?php echo $this->ec_html; ?>		
<?php endif;?>
