<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file
defined('_JEXEC') or die;
?>
<?php if($this->easycheckout_params->get('show_easycheckout_coupon',1)):?>
    <?php echo JText::_('EASYCHECKOUT_HAVE_COUPON');?> <a href="javascript:void ();" id="toggle_show_coupon_form"><?php echo JText::_('EASYCHECKOUT_APPLY_HERE'); ?></a></h4>
    <div id="easycheckout-coupon-form" class="coupon" style="display: none;">
        <form action="<?php echo JRoute::_('index.php'); ?>" method="post" enctype="multipart/form-data">
            <?php
            $coupon = F0FModel::getTmpInstance ( 'Coupons', 'J2StoreModel' )->get_coupon();
            ?>
            <div class="<?php echo $this->row_class; ?>">
                <div class="<?php echo $this->col_class;?>6">
                    <input type="text" name="coupon" value="<?php echo $coupon; ?>" />
                </div>
                <div class="<?php echo $this->col_class;?>6">
                    <input type="submit" value="<?php echo JText::_('J2STORE_APPLY_COUPON')?>" class="button btn btn-primary" />
                </div>
            </div>
            <input type="hidden" name="redirect" value="<?php echo base64_encode($this->checkout_url);?>">
            <input type="hidden" name="option" value="com_j2store" />
            <input type="hidden" name="view" value="carts" />
            <input type="hidden" name="task" value="applyCoupon" />
        </form>
    </div>
<?php endif; ?>
<?php if($this->easycheckout_params->get('show_easycheckout_voucher',0)):?>
    <br>
    <?php echo JText::_('EASYCHECKOUT_HAVE_VOUCHER');?> <a href="javascript:void ();" id="toggle_show_voucher_form"><?php echo JText::_('EASYCHECKOUT_APPLY_HERE'); ?></a></h4>
    <div id="easycheckout-voucher-form" style="display: none;" class="voucher">
        <form action="<?php echo JRoute::_('index.php'); ?>" method="post" enctype="multipart/form-data">
            <?php
            $voucher = F0FModel::getTmpInstance ( 'Vouchers', 'J2StoreModel' )->get_voucher();
            ?>
            <div class="<?php echo $this->row_class; ?>">
                <div class="<?php echo $this->col_class;?>6">
                    <input type="text" name="voucher" value="<?php echo $voucher; ?>" />
                </div>
                <div class="<?php echo $this->col_class;?>6">
                    <input type="submit" value="<?php echo JText::_('J2STORE_APPLY_VOUCHER')?>" class="button btn btn-primary" />
                </div>
            </div>
            <input type="hidden" name="redirect" value="<?php echo base64_encode($this->checkout_url);?>">
            <input type="hidden" name="option" value="com_j2store" />
            <input type="hidden" name="view" value="carts" />
            <input type="hidden" name="task" value="applyVoucher" />
        </form>
    </div>
<?php endif; ?>
