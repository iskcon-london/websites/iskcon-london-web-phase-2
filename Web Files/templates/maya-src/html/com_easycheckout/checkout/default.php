<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$show_form = true;
// check register allowed and guest checkout allowed
if($this->storeProfile->get( 'allow_registration', 1 ) == 0 && $this->storeProfile->get( 'allow_guest_checkout', 0 ) == 0 && $this->user->id < 1 ){
    $show_form = false;
}

?>

<div class="easy-checkout easy-checkout-checkout-form">
	<span class="easy-checkout-error-container"></span>

    <?php echo J2Store::modules()->loadposition( 'j2store-checkout-top' ); ?>

    <div class="easy-returning-customer easy-checkout-row">
        <?php if ( $this->params->get( 'show_login_form', 1 ) && $this->user->id < 1 ) : ?>
            <!-- let customer login -->
            <?php echo $this->loadTemplate( 'login_form' ); ?>
        <?php endif; ?>
    </div>
    <?php if($this->easycheckout_params->get('show_easycheckout_coupon', 1) || $this->easycheckout_params->get('show_easycheckout_voucher', 0)):?>
        <div class="easy-discount easy-checkout-row">
                <?php echo $this->loadTemplate('discount'); ?>
        </div>
    <?php endif; ?>
    <?php if($show_form):?>
    <div class="easy-checkout-row <?php echo $this->row_class; ?>">
        <div class="<?php echo $this->col_class; ?>6">
            <div class="easy-checkout-column">
                <h3 class="easy-checkout-title"><span>1</span> <?php echo JText::_('EASYCHECKOUT_CUSTOMER_INFORMATION'); ?></h3>

                        <?php echo $this->loadTemplate('address'); ?>

            </div>
        </div>
        <div class="<?php echo $this->col_class; ?>6">
            <div class="easy-checkout-column">
                <h3 class="easy-checkout-title"><span>2</span> <?php echo JText::_('EASYCHECKOUT_ORDER_OVERVIEW'); ?></h3>
                <div id="cart_refresh_block">
                    <?php echo $this->loadTemplate('cartsummary'); ?>
                    <?php echo $this->loadTemplate('shipping_payment'); ?>
                    <input type="button" class="btn btn-primary checkout-validate-button" value="<?php echo JText::_('J2STORE_CONTINUE'); ?>" />
                </div>
            </div>
        </div>
    </div>
        <div class="easy-checkout-confirm-payment <?php echo $this->row_class; ?>" style="display: none;">
            <div class="<?php echo $this->col_class; ?>12" id="easy-checkout-prepayment">

            </div>
        </div>
    <?php endif; ?>

</div>
