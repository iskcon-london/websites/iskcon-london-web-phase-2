<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */

// No direct access to this file
defined( '_JEXEC' ) or die;
?>

<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>6">[first_name]</div>
    <div class="<?php echo $this->col_class?>6">[last_name]</div>

</div>
<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>6">[email]</div>
    <div class="<?php echo $this->col_class?>6">[phone_2]</div>

</div>

<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>6">[address_1]</div>
    <div class="<?php echo $this->col_class?>6">[address_2]</div>

</div>

<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>6">[country_id]</div>
    <div class="<?php echo $this->col_class?>6">[city]</div>

</div>

<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>6">[zip]</div>
    <div class="<?php echo $this->col_class?>6">[zone_id]</div>

</div>

<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>6">[company]</div>
    <div class="<?php echo $this->col_class?>6">[phone_1]</div>

</div>
<div class="<?php echo $this->row_class; ?>">

    <div class="<?php echo $this->col_class?>12">[tax_number]</div>

</div>
