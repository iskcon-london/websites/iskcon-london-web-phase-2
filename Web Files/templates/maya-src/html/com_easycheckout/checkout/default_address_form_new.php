<?php
/**
 * @package     3.x
 * @subpackage  J2 Store Easy Checkout
 * @author      Alagesan, J2Store <support@j2store.org>
 * @copyright   Copyright (c) 2018 J2Store . All rights reserved.
 * @license     GNU GPL v3 or later
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */

// No direct access to this file
defined( '_JEXEC' ) or die;
$class = '';
if(!$this->showShipping){
    $class = 'hide';
}
?>
<div class="<?php echo $this->row_class; ?>">
    <div class="<?php echo $this->col_class?>12">
        <div id="easy-checkout-billing-address">
            <?php
            $html = $this->storeProfile->get('store_billing_layout');

            if(empty($html) || JString::strlen($html) < 5) {
                $html = $this->loadTemplate('address_format');
            }

            //first find all the checkout fields
            preg_match_all("^\[(.*?)\]^",$html,$checkoutFields, PREG_PATTERN_ORDER);
            $allFields = $this->fields;
            $status = false;

            ?>
            <?php foreach ($this->fields as $fieldName => $oneExtraField): ?>
                <?php
                if($fieldName == 'email') {
                    continue;
                }
                $onWhat='onchange'; if($oneExtraField->field_type=='radio') $onWhat='onclick';
                if(property_exists($this->address, $fieldName)) {
                    $placeholder =  (isset($oneExtraField->field_options['placeholder']) ? $oneExtraField->field_options['placeholder'] : "");
                    $field_options = '';
                    if($placeholder){
                        $field_options .= ' placeholder="'.$placeholder.'" ';
                    }
                    $html = str_replace('['.$fieldName.']',$this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, $fieldName,false, $field_options, $test = false, $allFields, $allValues = null).'</br />',$html);
                }
                ?>
            <?php endforeach; ?>


            <?php

            if($status == false) {
                //email not found. manually add it
                $user = JFactory::getUser();
                $email = '';
                if($user->id){
                    $email .= '<input type="hidden" name="email" id="email" value="'.$user->email.'"  />';
                }else{
                    $placeholder = '';
                    if(isset($this->fields['email']) && !empty($this->fields['email'])){
                        $placeholder =  (isset($this->fields['email']->field_options['placeholder']) ? $this->fields['email']->field_options['placeholder'] : "");
                    }
                    $email ='<span class="j2store_field_required">*</span><label for="email">'.JText::_('J2STORE_EMAIL').'</label>';
                    $email .='<input type="text" name="email" id="email" value="" class="large-field" placeholder="'.$placeholder.'" /> <br />';
                }

                $html = str_replace('[email]',$email,$html);
            }

            //re-check if email, password or confirm password fields are deleted. May be accidentally
            $phtml = '';

            if(!in_array('email', $checkoutFields[1]) && $status == false) {
                //it seems deleted. so process them
                $phtml .= $email;
            }


            $html = $html.$phtml;
            ?>



            <?php
            //check for unprocessed fields. If the user forgot to add the fields to the checkout layout in store profile, we probably have some.
            $unprocessedFields = array();
            foreach($this->fields as $fieldName => $oneExtraField) {
                if($fieldName == 'email') {
                    continue;
                }
                if(!in_array($fieldName, $checkoutFields[1])) {
                    $unprocessedFields[$fieldName] = $oneExtraField;
                }
            }

            //now we have unprocessed fields. remove any other square brackets found.
            preg_match_all("^\[(.*?)\]^",$html,$removeFields, PREG_PATTERN_ORDER);
            foreach($removeFields[1] as $fieldName) {
                $html = str_replace('['.$fieldName.']', '', $html);
            }

            ?>


            <?php echo $html; ?>

            <?php if(count($unprocessedFields)): ?>
                <div class="<?php echo $this->row_class;?>">
                    <div class="<?php echo $this->col_class;?>12">
                        <?php $uhtml = '';?>
                        <?php foreach ($unprocessedFields as $fieldName => $oneExtraField): ?>
                            <?php
                            $onWhat='onchange'; if($oneExtraField->field_type=='radio') $onWhat='onclick';
                            //echo $this->fieldsClass->display($oneExtraField,@$this->address->$fieldName,$fieldName,false);
                            if(property_exists($this->address, $fieldName)) {
                                $placeholder =  (isset($oneExtraField->field_options['placeholder']) ? $oneExtraField->field_options['placeholder'] : "");
                                $field_options = '';
                                if($placeholder){
                                    $field_options .= ' placeholder="'.$placeholder.'" ';
                                }
                                $uhtml .= $this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, $fieldName,false, $field_options, $test = false, $allFields, $allValues = null);
                                $uhtml .='<br />';
                            }
                            ?>
                        <?php endforeach; ?>
                        <?php echo $uhtml; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(!$this->user->id):?>
                <?php echo  $this->loadTemplate('register_account'); ?>
                <?php echo J2Store::plugin()->eventWithHtml('CheckoutGuest', array($this)); ?>
            <?php endif; ?>
            <br />

            <?php if($this->user->id): ?>
                <input type="hidden" name="email" value="<?php echo $this->user->email; ?>" />
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="<?php echo $this->row_class; ?> <?php echo $class;?>">
    <div class="<?php echo $this->col_class?>12"><input type="checkbox" id="same_as_billing_address" name="shipping_address" value="1" id="shipping" checked="checked" /><label for="shipping"><?php echo JText::_('J2STORE_MAKE_SHIPPING_SAME'); ?></label></div>
    <script>
        (function ($) {
            if($('#same_as_billing_address:checked').length == 0){
                $('#same_as_billing_address').trigger('click');
            }
        })(jQuery);
    </script>
</div>
<?php if($this->showShipping):?>
<div class="<?php echo $this->row_class; ?>">
    <div class="<?php echo $this->col_class?>12" id="easy-checkout-shipping-address" style="display: none;">
            <?php
            $html = $this->storeProfile->get('store_shipping_layout');

            if(empty($html) || JString::strlen($html) < 5) {
                $html = $this->loadTemplate('address_shipping_format');
            }

            //first find all the checkout fields
            preg_match_all("^\[(.*?)\]^",$html,$checkoutFields, PREG_PATTERN_ORDER);

            $allFields = $this->ship_fields;
            ?>
            <?php foreach ($this->ship_fields as $fieldName => $oneExtraField): ?>
                <?php

                $onWhat='onchange'; if($oneExtraField->field_type=='radio') $onWhat='onclick';
                if(property_exists($this->address, $fieldName) && $fieldName != 'email') {
                    $placeholder =  (isset($oneExtraField->field_options['placeholder']) ? $oneExtraField->field_options['placeholder'] : "");
                    $field_options = '';
                    if($placeholder){
                        $field_options .= ' placeholder="'.$placeholder.'" ';
                    }

                    $this->fieldsClass->prefix = 'shipping_';
                    $html = str_replace('['.$fieldName.']',$this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, 'shipping_'.$fieldName,false, $field_options, $test = false, $allFields, $allValues = null).'</br />',$html);
                }
                ?>
            <?php endforeach; ?>


            <?php
            $html = $html.$phtml;

            ?>



            <?php
            //check for unprocessed fields. If the user forgot to add the fields to the checkout layout in store profile, we probably have some.
            $unprocessedFields = array();
            foreach($this->ship_fields as $fieldName => $oneExtraField) {
                if(!in_array($fieldName, $checkoutFields[1])) {
                    $unprocessedFields[$fieldName] = $oneExtraField;
                }
            }

            //now we have unprocessed fields. remove any other square brackets found.
            preg_match_all("^\[(.*?)\]^",$html,$removeFields, PREG_PATTERN_ORDER);
            foreach($removeFields[1] as $fieldName) {
                $html = str_replace('['.$fieldName.']', '', $html);
            }

            ?>


            <?php echo $html; ?>

            <?php if(count($unprocessedFields)): ?>
                <div class="<?php echo $this->row_class;?>">
                    <div class="<?php echo $this->col_class;?>12">
                        <?php $uhtml = '';?>
                        <?php foreach ($unprocessedFields as $fieldName => $oneExtraField): ?>
                            <?php
                            $onWhat='onchange'; if($oneExtraField->field_type=='radio') $onWhat='onclick';
                            //echo $this->fieldsClass->display($oneExtraField,@$this->address->$fieldName,$fieldName,false);
                            if(property_exists($this->address, $fieldName) && $fieldName != 'email') {
                                $placeholder =  (isset($oneExtraField->field_options['placeholder']) ? $oneExtraField->field_options['placeholder'] : "");
                                $field_options = '';
                                if($placeholder){
                                    $field_options .= ' placeholder="'.$placeholder.'" ';
                                }

                                $this->fieldsClass->prefix = 'shipping_';
                                $uhtml .= $this->fieldsClass->getFormatedDisplay($oneExtraField,$this->address->$fieldName, 'shipping_'.$fieldName,false, $field_options, $test = false, $allFields, $allValues = null);
                                $uhtml .='<br />';
                            }
                            ?>
                        <?php endforeach; ?>
                        <?php echo $uhtml; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php if(!$this->user->id):?>
            <?php echo J2Store::plugin()->eventWithHtml('CheckoutGuestShipping', array($this)); ?>
        <?php endif; ?>
            <br />
    </div>
</div>
<?php endif; ?>
<style>
    #country_id {
     display: block !important;
    }
    #country_id_chzn {
        display: none;
    }
    #zone_id {
        display: block !important;
    }
    #zone_id_chzn {
        display: none;
    }
    #shipping_country_id {
        display: block !important;
    }
    #shipping_country_id_chzn {
        display: none;
    }
    #shipping_zone_id {
        display: block !important;
    }
    #shipping_zone_id_chzn {
        display: none;
    }
</style>





