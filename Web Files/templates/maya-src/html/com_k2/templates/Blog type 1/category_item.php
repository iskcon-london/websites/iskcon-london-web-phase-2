<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

?>
<?php if($this->k % 2 == 0){?> 
<div class="col-1-2">
                
    <div class="blog-box padd-25">
        
        <div class="blog-thumb" style="background-image: url('<?php echo $this->item->image;?>');">
                                        
            <div class="blog-caption transit">
                <div class="blog-more nd-clr"><a href="<?php echo $this->item->link; ?>"><?php echo JText::_('K2_READ_MORE'); ?></a></div>
                <div class="blog-icon"><i class="icon-paper"></i></div>
                <div class="blog-date marg-bott-25 fs-col">
                <?php if($this->item->params->get('catItemDateCreated')): ?>
                <div class="blog-date marg-bott-25 fs-clr">
            	<!-- Date created -->
            		<?php echo JHTML::_('date', $this->item->created , 'd - M - Y'); ?>
            	
                </div>
            	<?php endif; ?>
                </div>
            </div>
            
        </div>
        
    </div>
    
</div>

<div class="col-1-2">

    <div class="blog-box padd-25">
        
        <div class="blog-detail marg-top-50">
        
            <div class="blog-title padd-x-25 fs-clr">
                <a href="<?php echo $this->item->link; ?>"><?php echo $this->item->title; ?></a>
            </div>
                                        
            <div class="blog-meta">
            <?php if($this->item->params->get('catItemAuthor')): ?>
            <span class="blog-author"><?php echo $this->item->author->name; ?></span>
        	<?php endif;?>	 / 
                <span class="blog-cat">
                <?php if($this->item->params->get('catItemCategory')): ?>
        		<!-- Item category name -->
        			<span><?php //echo JText::_('K2_PUBLISHED_IN'); ?></span>
        			<a href="<?php echo $this->item->category->link; ?>"> <?php echo $this->item->category->name; ?></a>
        	
        		<?php endif; ?>
                 / 
                </span>
            </div>
            
            <div class="line-center line-grey"></div>
            
            <div class="blog-exc padd-x-25">
                 <?php if($this->item->params->get('catItemIntroText')): ?>
            	  <!-- Item introtext -->
            	  <div class="btcatItemIntroText">
            	  	<?php echo $this->item->introtext; ?>
            	  </div>
            	  <?php endif; ?>
            </div>
        </div>
        
    </div>
    
</div>
<?php }else{ ?>

<div class="col-1-2">

    <div class="blog-box padd-25">
        
        <div class="blog-detail marg-top-50">
        
            <div class="blog-title padd-x-25 fs-clr">
                <a href="<?php echo $this->item->link; ?>"><?php echo $this->item->title; ?></a>
            </div>
                                        
            <div class="blog-meta">
            <?php if($this->item->params->get('catItemAuthor')): ?>
            <span class="blog-author"><?php echo $this->item->author->name; ?></span>
        	<?php endif;?>	 / 
                <span class="blog-cat">
                <?php if($this->item->params->get('catItemCategory')): ?>
        		<!-- Item category name -->
        			<span><?php //echo JText::_('K2_PUBLISHED_IN'); ?></span>
        			<a href="<?php echo $this->item->category->link; ?>"> <?php echo $this->item->category->name; ?></a>
        	
        		<?php endif; ?>
                 / 
                </span>
            </div>
            
            <div class="line-center line-grey"></div>
            
            <div class="blog-exc padd-x-25">
                 <?php if($this->item->params->get('catItemIntroText')): ?>
            	  <!-- Item introtext -->
            	  <div class="btcatItemIntroText">
            	  	<?php echo $this->item->introtext; ?>
            	  </div>
            	  <?php endif; ?>
            </div>
        </div>
        
    </div>
    
</div>
<div class="col-1-2">
                
    <div class="blog-box padd-25">
        
        <div class="blog-thumb" style="background-image: url('<?php echo $this->item->image;?>');">
                                        
            <div class="blog-caption transit">
                <div class="blog-more nd-clr"><a href="<?php echo $this->item->link; ?>">Read More</a></div>
                <div class="blog-icon"><i class="icon-paper"></i></div>
                <div class="blog-date marg-bott-25 fs-col">
                <?php if($this->item->params->get('catItemDateCreated')): ?>
                <div class="blog-date marg-bott-25 fs-clr">
            	<!-- Date created -->
            		<?php echo JHTML::_('date', $this->item->created , JText::_('K2_DATE_FORMAT_LC2')); ?>
            	
                </div>
            	<?php endif; ?>
                </div>
            </div>
            
        </div>
        
    </div>
    
</div>

 <?php }?>

<ul class="post-tags">
	
        

		
        	<?php if($this->item->params->get('catItemHits')): ?>
		<!-- Item Hits -->
		<li>
				<?php //echo JText::_('K2_READ'); ?><i class="fa fa-heart"></i> <b><?php echo $this->item->hits; ?></b> <?php echo JText::_('K2_TIMES'); ?>
			
		</li>
		<?php endif; ?>
	<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
	<!-- Anchor link to comments below -->
	<li>
		<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
			<!-- K2 Plugins: K2CommentsCounter -->
			<?php echo $this->item->event->K2CommentsCounter; ?>
		<?php else: ?>
			<?php if($this->item->numOfComments > 0): ?>
			<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
				<i class="fa fa-comments"></i><?php echo $this->item->numOfComments; ?> <?php echo ($this->item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
			</a>
			<?php else: ?>
			<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
				<i class="fa fa-comments"></i><?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
			</a>
			<?php endif; ?>
		<?php endif; ?>
	</li>
	<?php endif; ?>
</ul>

<!-- Start K2 Item Layout -->
<div class="btcatItemView btgroup<?php echo ucfirst($this->item->itemGroup); ?><?php echo ($this->item->featured) ? ' catItemIsFeatured' : ''; ?>">

	<!-- Plugins: BeforeDisplay -->
	<?php echo $this->item->event->BeforeDisplay; ?>

	<!-- K2 Plugins: K2BeforeDisplay -->
	<?php echo $this->item->event->K2BeforeDisplay; ?>

	<div class="catItemHeader">
		
	  

	
  </div>

  <!-- Plugins: AfterDisplayTitle -->
  <?php echo $this->item->event->AfterDisplayTitle; ?>

  <!-- K2 Plugins: K2AfterDisplayTitle -->
  <?php echo $this->item->event->K2AfterDisplayTitle; ?>

	<?php if($this->item->params->get('catItemRating')): ?>
	<!-- Item Rating -->
	<div class="catItemRatingBlock">
		<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
		<div class="itemRatingForm">
			<ul class="itemRatingList">
				<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
			</ul>
			<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
			<div class="clr"></div>
		</div>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

  <div class="catItemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  <?php echo $this->item->event->BeforeDisplayContent; ?>

	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  <?php echo $this->item->event->K2BeforeDisplayContent; ?>

	  

	  <?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
	  <!-- Item extra fields -->
	  <div class="catItemExtraFields">
	  	<h4><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></h4>
	  	<ul>
			<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
			<?php if($extraField->value != ''): ?>
			<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
				<?php if($extraField->type == 'header'): ?>
				<h4 class="catItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
				<?php else: ?>
				<span class="catItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
				<span class="catItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
				<?php endif; ?>
			</li>
			<?php endif; ?>
			<?php endforeach; ?>
			</ul>
	    <div class="clr"></div>
	  </div>
	  <?php endif; ?>

	  <!-- Plugins: AfterDisplayContent -->
	  <?php echo $this->item->event->AfterDisplayContent; ?>

	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  <?php echo $this->item->event->K2AfterDisplayContent; ?>

	  <div class="clr"></div>
  </div>

  <?php if(
  $this->item->params->get('catItemTags') ||
  $this->item->params->get('catItemAttachments')
  ): ?>
  <div class="catItemLinks">

	

	  <?php if($this->item->params->get('catItemTags') && count($this->item->tags)): ?>
	  <!-- Item tags -->
	  <div class="catItemTagsBlock">
		  <span><?php echo JText::_('K2_TAGGED_UNDER'); ?></span>
		  <ul class="catItemTags">
		    <?php foreach ($this->item->tags as $tag): ?>
		    <li><a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a></li>
		    <?php endforeach; ?>
		  </ul>
		  <div class="clr"></div>
	  </div>
	  <?php endif; ?>

	  <?php if($this->item->params->get('catItemAttachments') && count($this->item->attachments)): ?>
	  <!-- Item attachments -->
	  <div class="catItemAttachmentsBlock">
		  <span><?php echo JText::_('K2_DOWNLOAD_ATTACHMENTS'); ?></span>
		  <ul class="catItemAttachments">
		    <?php foreach ($this->item->attachments as $attachment): ?>
		    <li>
			    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>">
			    	<?php echo $attachment->title ; ?>
			    </a>
			    <?php if($this->item->params->get('catItemAttachmentsCounter')): ?>
			    <span>(<?php echo $attachment->hits; ?> <?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>)</span>
			    <?php endif; ?>
		    </li>
		    <?php endforeach; ?>
		  </ul>
	  </div>
	  <?php endif; ?>

		<div class="clr"></div>
  </div>
  <?php endif; ?>

	<div class="clr"></div>

  <?php if($this->item->params->get('catItemVideo') && !empty($this->item->video)): ?>
  <!-- Item video -->
  <div class="catItemVideoBlock">
  	<h3><?php echo JText::_('K2_RELATED_VIDEO'); ?></h3>
		<?php if($this->item->videoType=='embedded'): ?>
		<div class="catItemVideoEmbedded">
			<?php echo $this->item->video; ?>
		</div>
		<?php else: ?>
		<span class="catItemVideo"><?php echo $this->item->video; ?></span>
		<?php endif; ?>
  </div>
  <?php endif; ?>

  <?php if($this->item->params->get('catItemImageGallery') && !empty($this->item->gallery)): ?>
  <!-- Item image gallery -->
  <div class="catItemImageGallery">
	  <h4><?php echo JText::_('K2_IMAGE_GALLERY'); ?></h4>
	  <?php echo $this->item->gallery; ?>
  </div>
  <?php endif; ?>

	<?php if($this->item->params->get('catItemDateModified')): ?>
	<!-- Item date modified -->
	<?php if($this->item->modified != $this->nullDate && $this->item->modified != $this->item->created ): ?>
	<span class="catItemDateModified">
		<?php echo JText::_('K2_LAST_MODIFIED_ON'); ?> <?php echo JHTML::_('date', $this->item->modified, JText::_('K2_DATE_FORMAT_LC2')); ?>
	</span>
	<?php endif; ?>
	<?php endif; ?>

  <!-- Plugins: AfterDisplay -->
  <?php echo $this->item->event->AfterDisplay; ?>

  <!-- K2 Plugins: K2AfterDisplay -->
  <?php echo $this->item->event->K2AfterDisplay; ?>

	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
