<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
$st_item = Bluejoomla::loadK2Item($this->item->id);
$st_extra = json_decode($st_item->extra_fields);
$item_count = count($st_extra) - 3;
$check_gallery = $st_extra[0]->value;
$check_slider = false;
foreach($st_extra as $k=>$val){
    if ($k== 0) continue;
    if($val->value != '') {
        $check_slider = true;
        break;
    }
}
?>
<?php if($check_gallery != ''){
require_once JPATH_ROOT.'/modules/mod_blue_design/helper.php';
$images = ModEvolveSliderHelper::getImageDir($check_gallery);
sort($images);
?>

<div class="grid-content marg-top-100">
            
        <div class="gallery-list row">
            <?php foreach($images as $k=>$val):?>
        	<div class="gallery-box col-1-3">
                                    
                <div class="gallery-thumb" style="background-image: url('<?php echo JUri::root().$val; ?>');">
                                                                
                    <div class="gallery-caption transit">
                        <div class="gallery-icon"><a href="<?php echo JUri::root().$val; ?>" title="<?php echo $this->item->title; ?>" rel="category" class="lightbox"><i class="icon-zoom-in"></i></a></div>
                    </div>
                    
                </div>
            
            </div>
            <?php endforeach;?>
         </div>
</div>
<div class="header-page padd-y-75 bg-gr1-clr" style="clear: both;">
            
    <div class="title-page padd-x-25 fs-clr"><h3><?php echo $this->item->title; ?></h3></div>
    <div class="meta-page padd-x-25 fs-clr">
    <?php if($this->item->params->get('itemAuthor')): ?>
	<!-- Item Author -->
		<?php echo K2HelperUtilities::writtenBy($this->item->author->profile->gender); ?>&nbsp;
		<?php if(empty($this->item->created_by_alias)): ?>
		<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i><?php echo $this->item->author->name; ?></a>
		<?php else: ?>
		<?php echo $this->item->author->name; ?>
		<?php endif; ?>
	<?php endif; ?>
    /
		<?php if($this->item->params->get('itemDateCreated')): ?>
		<!-- Date created -->
			<i class="fa fa-calendar"></i> <?php echo JHTML::_('date', $this->item->created , JText::_('K2_DATE_FORMAT_LC2')); ?>
		
		<?php endif; ?>
        /
        <?php if($this->item->params->get('itemCategory')): ?>
		<!-- Item category -->
			<span><?php //echo JText::_('K2_PUBLISHED_IN'); ?></span>
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-eye"></i><?php echo $this->item->category->name; ?></a>
	
		<?php endif; ?>
   
    </div>
    
</div>
<?php } ?>
<?php if(!empty($this->item->fulltext)): ?>
  <?php if($this->item->params->get('itemIntroText')): ?>
  <!-- Item introtext -->
  <div class="btitemIntroText">
  	<?php echo $this->item->introtext; ?>
  </div>
  <?php endif; ?>
  <?php if($this->item->params->get('itemFullText')): ?>
  <!-- Item fulltext -->
  <div class="btitemFullText">
  	<?php echo $this->item->fulltext; ?>
  </div>
  <?php endif; ?>
  <?php else: ?>
  <!-- Item text -->
  <div class="btitemFullText">
  	<?php echo $this->item->introtext; ?>
  </div>
  <?php endif; ?>
<!-- Item navigation -->
<div class="port-nav bg-fs-clr padd-25">
        
	<ul>
    
    	<li class="port-prev brd-wh-clr wh-clr bg-wh-clr-hov fs-clr-hov">
            <a href="<?php echo $this->item->previousLink; ?>"><i class="icon-arrow-left"></i></a>
        </li>
        
        <li class="port-home brd-wh-clr wh-clr bg-wh-clr-hov fs-clr-hov">
            <a href="#"><i class="icon-menu"></i></a>
        </li>
                
        <li class="port-next brd-wh-clr wh-clr bg-wh-clr-hov fs-clr-hov">
            <a href="<?php echo $this->item->nextLink; ?>"><i class="icon-arrow-right"></i></a>
        </li>
    
    </ul>

</div>