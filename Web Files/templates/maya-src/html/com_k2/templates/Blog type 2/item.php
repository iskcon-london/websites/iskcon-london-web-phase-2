<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

$bj_protocol = $_SERVER['SERVER_PROTOCOL'];
$bj_protocol_array = explode('/',$bj_protocol);
if(isset($_SERVER['REDIRECT_URL'])){
    
$link = strtolower($bj_protocol_array[0]).'://'.$_SERVER['SERVER_NAME'].$_SERVER['REDIRECT_URL'];
}elseif(isset($_SERVER['PHP_SELF'])){
    
$link = strtolower($bj_protocol_array[0]).'://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
}

$st_item = Bluejoomla::loadK2Item($this->item->id);
$st_extra = json_decode($st_item->extra_fields);
$item_count = count($st_extra) - 3;
$check_gallery = $st_extra[0]->value;
$check_slider = false;
foreach($st_extra as $k=>$val){
    if ($k== 0) continue;
    if($val->value != '') {
        $check_slider = true;
        break;
    }
}
?>


<?php if($check_gallery != '' && $check_gallery != 'none'){
require_once JPATH_ROOT.'/modules/mod_blue_design/helper.php';
$images = ModEvolveSliderHelper::getImageDir($check_gallery);
sort($images);
?>

<div class="grid-content marg-top-100">
            
        <div class="gallery-list row">
            <?php foreach($images as $k=>$val):?>
        	<div class="gallery-box col-1-3">
                                    
                <div class="gallery-thumb" style="background-image: url('<?php echo JUri::root().$val; ?>');">
                                                                
                    <div class="gallery-caption transit">
                        <div class="gallery-icon"><a title="<?php echo $this->item->title; ?>" href="<?php echo JUri::root().$val; ?>" rel="category" class="lightbox"><i class="icon-zoom-in"></i></a></div>
                    </div>
                    
                </div>
            
            </div>
            <?php endforeach;?>
         </div>
</div>
<div class="header-page padd-y-75 bg-gr1-clr" style="clear: both;">
            
    <div class="title-page padd-x-25 fs-clr"><h3><?php echo $this->item->title; ?></h3></div>
    <div class="meta-page padd-x-25 fs-clr">
    <?php if($this->item->params->get('itemAuthor')): ?>
	<!-- Item Author -->
		<?php echo K2HelperUtilities::writtenBy($this->item->author->profile->gender); ?>&nbsp;
		<?php if(empty($this->item->created_by_alias)): ?>
		<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i><?php echo $this->item->author->name; ?></a>
		<?php else: ?>
		<?php echo $this->item->author->name; ?>
		<?php endif; ?>
	<?php endif; ?>
    /
		<?php if($this->item->params->get('itemDateCreated')): ?>
		<!-- Date created -->
			<i class="fa fa-calendar"></i> <?php echo JHTML::_('date', $this->item->created , JText::_('K2_DATE_FORMAT_LC2')); ?>
		
		<?php endif; ?>
        /
        <?php if($this->item->params->get('itemCategory')): ?>
		<!-- Item category -->
			<span><?php //echo JText::_('K2_PUBLISHED_IN'); ?></span>
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-eye"></i><?php echo $this->item->category->name; ?></a>
	
		<?php endif; ?>
   
    </div>
    
</div>
<?php }elseif($check_slider){?>
<div id="post-carousel">
    <?php foreach($st_extra as $key=>$val): if ($key == 0 || $val->value == '') continue; ?>
	<div class="slide-image" style="background-image: url('<?php echo JUri::base(false).$val->value; ?>');">
        <div class="intro-message">
        
            <div class="intro-title padd-x-50"></div>
            <div class="intro-subtitle marg-x-50 bg-fs-clr"></div>
                               
        </div>
    </div>
    <?php endforeach;?>
</div>

<div class="header-page padd-y-75 bg-gr1-clr" style="clear: both;">
            
    <div class="title-page padd-x-25 fs-clr"><h3><?php echo $this->item->title; ?></h3></div>
    <div class="meta-page padd-x-25 fs-clr">
    <?php if($this->item->params->get('itemAuthor')): ?>
	<!-- Item Author -->
		<?php echo K2HelperUtilities::writtenBy($this->item->author->profile->gender); ?>&nbsp;
		<?php if(empty($this->item->created_by_alias)): ?>
		<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i><?php echo $this->item->author->name; ?></a>
		<?php else: ?>
		<?php echo $this->item->author->name; ?>
		<?php endif; ?>
	<?php endif; ?>
    /
		<?php if($this->item->params->get('itemDateCreated')): ?>
		<!-- Date created -->
			<i class="fa fa-calendar"></i> <?php echo JHTML::_('date', $this->item->created , JText::_('K2_DATE_FORMAT_LC2')); ?>
		
		<?php endif; ?>
        /
        <?php if($this->item->params->get('itemCategory')): ?>
		<!-- Item category -->
			<span><?php //echo JText::_('K2_PUBLISHED_IN'); ?></span>
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-eye"></i><?php echo $this->item->category->name; ?></a>
	
		<?php endif; ?>
   
    </div>
    
</div>

<?php }else{?>

<?php if($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
	  <div id="post-image" class="row" style="background-image: url('<?php echo $this->item->image; ?>')">
        
        <div class="header-page padd-y-75">
    
            <div class="header-layer-page padd-y-75 bg-wh-alpha">
            
                <div class="title-page padd-x-25 fs-clr"><h1 class="intro-title padd-x-50"><?php echo $this->item->title; ?></h1></div>
                <div class="meta-page padd-x-25 white-color">
    <?php if($this->item->params->get('itemAuthor')): ?>
	<!-- Item Author -->
		<?php echo K2HelperUtilities::writtenBy($this->item->author->profile->gender); ?>&nbsp;
		<?php if(empty($this->item->created_by_alias)): ?>
		<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i><?php echo $this->item->author->name; ?></a>
		<?php else: ?>
		<?php echo $this->item->author->name; ?>
		<?php endif; ?>
	<?php endif; ?>
    /
		<?php if($this->item->params->get('itemDateCreated')): ?>
		<!-- Date created -->
			<i class="fa fa-calendar"></i> <?php echo JHTML::_('date', $this->item->created , JText::_('K2_DATE_FORMAT_LC2')); ?>
		
		<?php endif; ?>
        /
        <?php if($this->item->params->get('itemCategory')): ?>
		<!-- Item category -->
			<span><?php //echo JText::_('K2_PUBLISHED_IN'); ?></span>
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-eye"></i><?php echo $this->item->category->name; ?></a>
	
		<?php endif; ?>
   
    </div>
                
            </div>
            
        </div>
        
    </div>
      
	  <?php endif; ?>
<?php } ?>


<?php if(JRequest::getInt('print')==1): ?>
<!-- Print button at the top of the print page only -->
<a class="itemPrintThisPage" rel="nofollow" href="#" onclick="window.print();return false;">
	<span><?php echo JText::_('K2_PRINT_THIS_PAGE'); ?></span>
</a>
<?php endif; ?>

<!-- Start K2 Item Layout -->
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div class="post-wrap padd-y-50 boxed">
<article>
    <?php if($this->item->params->get('itemVideo') && !empty($this->item->video)): ?>
  <!-- Item video -->
  <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

  <div class="itemVideoBlock">
  	<h3><?php echo JText::_('K2_MEDIA'); ?></h3>

		<?php if($this->item->videoType=='embedded'): ?>
		<div class="itemVideoEmbedded">
			<?php echo $this->item->video; ?>
		</div>
		<?php else: ?>
		<span class="itemVideo"><?php echo $this->item->video; ?></span>
		<?php endif; ?>

	  <?php if($this->item->params->get('itemVideoCaption') && !empty($this->item->video_caption)): ?>
	  <span class="itemVideoCaption"><?php echo $this->item->video_caption; ?></span>
	  <?php endif; ?>

	  <?php if($this->item->params->get('itemVideoCredits') && !empty($this->item->video_credits)): ?>
	  <span class="itemVideoCredits"><?php echo $this->item->video_credits; ?></span>
	  <?php endif; ?>

	  <div class="clr"></div>
  </div>
  <?php endif; ?>
	<!-- Plugins: BeforeDisplay -->
	<?php echo $this->item->event->BeforeDisplay; ?>

	<!-- K2 Plugins: K2BeforeDisplay -->
	<?php echo $this->item->event->K2BeforeDisplay; ?>
    <div class="post-content padd-x-25">
        
            
            <?php if(!empty($this->item->fulltext)): ?>
    	  <?php if($this->item->params->get('itemIntroText')): ?>
    	  <!-- Item introtext -->
    	  <div class="btitemIntroText">
    	  	<?php echo $this->item->introtext; ?>
    	  </div>
    	  <?php endif; ?>
    	  <?php if($this->item->params->get('itemFullText')): ?>
    	  <!-- Item fulltext -->
    	  <div class="btitemFullText">
    	  	<?php echo $this->item->fulltext; ?>
    	  </div>
    	  <?php endif; ?>
    	  <?php else: ?>
    	  <!-- Item text -->
    	  <div class="btitemFullText">
    	  	<?php echo $this->item->introtext; ?>
    	  </div>
    	  <?php endif; ?>
            
        </div>
        <div class="line-center gr2-clr"></div>
<div >
	<div class="containers">
		
		<ul class="post-tags">
            
            	<?php if($this->item->params->get('itemHits')): ?>
			<!-- Item Hits -->
			<li>
				<?php //echo JText::_('K2_READ'); ?> <i class="fa fa-heart"></i><b><?php echo $this->item->hits; ?></b> <?php echo JText::_('K2_TIMES'); ?>
			</li>
			<?php endif; ?>
<?php if($this->item->params->get('itemCommentsAnchor') && $this->item->params->get('itemComments') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
			<!-- Anchor link to comments below - if enabled -->
			<li>
				<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $this->item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($this->item->numOfComments > 0): ?>
					<a class="itemCommentsLink k2Anchor" href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
						<i class="fa fa-comments"></i><span><?php echo $this->item->numOfComments; ?></span> <?php echo ($this->item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
					</a>
					<?php else: ?>
					<a class="itemCommentsLink k2Anchor" href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
						<i class="fa fa-comments"></i><?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			</li>
			<?php endif; ?>
		</ul>
		

	  
	</div>
</div>
<div class="post-tags-share">
	<div class="container">
        

	  <?php if($this->item->params->get('itemTags') && count($this->item->tags)): ?>
	  <!-- Item tags -->
	  <div class="post-tags-box">
		  <h2><span><?php echo JText::_('K2_TAGGED_UNDER'); ?></span></h2>
		  <ul class="btitemTags">
		    <?php foreach ($this->item->tags as $tag): ?>
		    <li><a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a></li>
		    <?php endforeach; ?>
		  </ul>
	  </div>
	  <?php endif; ?>
		<div class="post-share-box">
			<ul>
                <?php if($this->item->params->get('itemTwitterButton',1)): ?>
            		<!-- Twitter Button -->
                    
            		<li class="">
            			<a  class="twitter" data-toggle="tooltip" data-original-title="Twitter" href="https://twitter.com/share"  data-count="horizontal"<?php if($this->item->params->get('twitterUsername')): ?> data-via="<?php echo $this->item->params->get('twitterUsername'); ?>"<?php endif; ?>>
            			<i class="fa fa-twitter"></i></a><?php //echo JText::_('K2_TWEET'); ?>
            			</a>
            			<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
            		</li>
            		<?php endif; ?>
            
            		<?php if($this->item->params->get('itemFacebookButton',1)): ?>
            		<!-- Facebook Button -->
            		<li><a class="facebook" data-toggle="tooltip" data-original-title="Facebook" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $link; ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="<?php echo $link; ?>">
                    <i class="fa fa-facebook"></i></a></a></li>
            		
            		<?php endif; ?>
            
            	<!--	<li><a class="linkedin" data-original-title="Linkedin" data-toggle="tooltip" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $link; ?>','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $link; ?>"><i class="fa fa-linkedin"></i></a></a></li>
                    -->
			</ul>
		</div>						
	</div>
</div>

  <?php if($this->item->params->get('itemAuthorBlock') && empty($this->item->created_by_alias)): ?>
  <!-- Author Block -->
  <div class="admin-place">
<div class="container">
  	<?php if($this->item->params->get('itemAuthorImage') && !empty($this->item->author->avatar)): ?>
  	<img class="itemAuthorAvatar" src="<?php echo $this->item->author->avatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($this->item->author->name); ?>" />
  	<?php endif; ?>

    <div class="admin-text-content">
      <h2 class="itemAuthorName">
      	<a rel="author" href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
      </h2>

      <?php if($this->item->params->get('itemAuthorDescription') && !empty($this->item->author->profile->description)): ?>
      <p><?php echo $this->item->author->profile->description; ?></p>
      <?php endif; ?>

      <?php if($this->item->params->get('itemAuthorURL') && !empty($this->item->author->profile->url)): ?>
      <span class="itemAuthorUrl"><?php echo JText::_('K2_WEBSITE'); ?> <a rel="me" href="<?php echo $this->item->author->profile->url; ?>" target="_blank"><?php echo str_replace('http://','',$this->item->author->profile->url); ?></a></span>
      <?php endif; ?>

      <?php if($this->item->params->get('itemAuthorEmail')): ?>
      <span class="itemAuthorEmail"><?php echo JText::_('K2_EMAIL'); ?> <?php echo JHTML::_('Email.cloak', $this->item->author->email); ?></span>
      <?php endif; ?>

			<div class="clr"></div>

			<!-- K2 Plugins: K2UserDisplay -->
			<?php echo $this->item->event->K2UserDisplay; ?>

    </div>
</div>  </div>
  <?php endif; ?>

  <!-- Plugins: AfterDisplayTitle -->
  <?php echo $this->item->event->AfterDisplayTitle; ?>

  <!-- K2 Plugins: K2AfterDisplayTitle -->
  <?php echo $this->item->event->K2AfterDisplayTitle; ?>

	<?php if(
		$this->item->params->get('itemFontResizer') ||
		$this->item->params->get('itemPrintButton') ||
		$this->item->params->get('itemEmailButton') ||
		$this->item->params->get('itemSocialButton') ||
		$this->item->params->get('itemVideoAnchor') ||
		$this->item->params->get('itemImageGalleryAnchor') ||
		$this->item->params->get('itemCommentsAnchor')
	): ?>
  <div class="btitemToolbar">
		<ul>
			<?php if($this->item->params->get('itemFontResizer')): ?>
			<!-- Font Resizer -->
			<li>
				<span class="itemTextResizerTitle"><?php echo JText::_('K2_FONT_SIZE'); ?></span>
				<a href="#" id="fontDecrease">
					<span><?php echo JText::_('K2_DECREASE_FONT_SIZE'); ?></span>
					<img src="<?php echo JURI::root(true); ?>/components/com_k2/images/system/blank.gif" alt="<?php echo JText::_('K2_DECREASE_FONT_SIZE'); ?>" />
				</a>
				<a href="#" id="fontIncrease">
					<span><?php echo JText::_('K2_INCREASE_FONT_SIZE'); ?></span>
					<img src="<?php echo JURI::root(true); ?>/components/com_k2/images/system/blank.gif" alt="<?php echo JText::_('K2_INCREASE_FONT_SIZE'); ?>" />
				</a>
			</li>
			<?php endif; ?>

			<?php if($this->item->params->get('itemPrintButton') && !JRequest::getInt('print')): ?>
			<!-- Print Button -->
			<li>
				<a class="itemPrintLink" rel="nofollow" href="<?php echo $this->item->printLink; ?>" onclick="window.open(this.href,'printWindow','width=900,height=600,location=no,menubar=no,resizable=yes,scrollbars=yes'); return false;">
					<span><?php echo JText::_('K2_PRINT'); ?></span>
				</a>
			</li>
			<?php endif; ?>

			<?php if($this->item->params->get('itemEmailButton') && !JRequest::getInt('print')): ?>
			<!-- Email Button -->
			<li>
				<a class="itemEmailLink" rel="nofollow" href="<?php echo $this->item->emailLink; ?>" onclick="window.open(this.href,'emailWindow','width=400,height=350,location=no,menubar=no,resizable=no,scrollbars=no'); return false;">
					<span><?php echo JText::_('K2_EMAIL'); ?></span>
				</a>
			</li>
			<?php endif; ?>

			<?php if($this->item->params->get('itemSocialButton') && !is_null($this->item->params->get('socialButtonCode', NULL))): ?>
			<!-- Item Social Button -->
			<li>
				<?php echo $this->item->params->get('socialButtonCode'); ?>
			</li>
			<?php endif; ?>

			
			<?php if($this->item->params->get('itemImageGalleryAnchor') && !empty($this->item->gallery)): ?>
			<!-- Anchor link to item image gallery below - if it exists -->
			<li>
				<a class="itemImageGalleryLink k2Anchor" href="<?php echo $this->item->link; ?>#itemImageGalleryAnchor"><?php echo JText::_('K2_IMAGE_GALLERY'); ?></a>
			</li>
			<?php endif; ?>

			
		</ul>
		<div class="clr"></div>
  </div>
	<?php endif; ?>

	<?php if($this->item->params->get('itemRating')): ?>
	<!-- Item Rating -->
	<div class="itemRatingBlock">
		<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
		<div class="itemRatingForm">
			<ul class="itemRatingList">
				<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
				<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
			</ul>
			<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
			<div class="clr"></div>
		</div>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

  <div class="btitemBody">

	  <!-- Plugins: BeforeDisplayContent -->
	  <?php echo $this->item->event->BeforeDisplayContent; ?>

	  <!-- K2 Plugins: K2BeforeDisplayContent -->
	  <?php echo $this->item->event->K2BeforeDisplayContent; ?>

	  

	  
		<?php if(($this->item->params->get('itemDateModified') && intval($this->item->modified)!=0)): ?>
		<div class="itemContentFooter">

		
			<?php if($this->item->params->get('itemDateModified') && intval($this->item->modified)!=0): ?>
			<!-- Item date modified -->
			<span class="itemDateModified">
				<?php echo JText::_('K2_LAST_MODIFIED_ON'); ?> <?php echo JHTML::_('date', $this->item->modified, JText::_('K2_DATE_FORMAT_LC2')); ?>
			</span>
			<?php endif; ?>

			<div class="clr"></div>
		</div>
		<?php endif; ?>

	  <!-- Plugins: AfterDisplayContent -->
	  <?php echo $this->item->event->AfterDisplayContent; ?>

	  <!-- K2 Plugins: K2AfterDisplayContent -->
	  <?php echo $this->item->event->K2AfterDisplayContent; ?>

	  <div class="clr"></div>
  </div>


  <?php if($this->item->params->get('itemAttachments')): ?>
  <div class="btitemLinks">

		

	  <?php if($this->item->params->get('itemAttachments') && count($this->item->attachments)): ?>
	  <!-- Item attachments -->
	  <div class="btitemAttachmentsBlock">
		  <span><?php echo JText::_('K2_DOWNLOAD_ATTACHMENTS'); ?></span>
		  <ul class="itemAttachments">
		    <?php foreach ($this->item->attachments as $attachment): ?>
		    <li>
			    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>
			    <?php if($this->item->params->get('itemAttachmentsCounter')): ?>
			    <span>(<?php echo $attachment->hits; ?> <?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>)</span>
			    <?php endif; ?>
		    </li>
		    <?php endforeach; ?>
		  </ul>
	  </div>
	  <?php endif; ?>

		<div class="clr"></div>
  </div>
  <?php endif; ?>

  <?php if($this->item->params->get('itemAuthorLatest') && empty($this->item->created_by_alias) && isset($this->authorLatestItems)): ?>
  <!-- Latest items from author -->
	<div class="itemAuthorLatest">
		<h3><?php echo JText::_('K2_LATEST_FROM'); ?> <?php echo $this->item->author->name; ?></h3>
		<ul>
			<?php foreach($this->authorLatestItems as $key=>$item): ?>
			<li class="<?php echo ($key%2) ? "odd" : "even"; ?>">
				<a href="<?php echo $item->link ?>"><?php echo $item->title; ?></a>
			</li>
			<?php endforeach; ?>
		</ul>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

	<?php
	/*
	Note regarding 'Related Items'!
	If you add:
	- the CSS rule 'overflow-x:scroll;' in the element div.itemRelated {â€¦} in the k2.css
	- the class 'k2Scroller' to the ul element below
	- the classes 'k2ScrollerElement' and 'k2EqualHeights' to the li element inside the foreach loop below
	- the style attribute 'style="width:<?php echo $item->imageWidth; ?>px;"' to the li element inside the foreach loop below
	...then your Related Items will be transformed into a vertical-scrolling block, inside which, all items have the same height (equal column heights). This can be very useful if you want to show your related articles or products with title/author/category/image etc., which would take a significant amount of space in the classic list-style display.
	*/
	?>

  <?php if($this->item->params->get('itemRelated') && isset($this->relatedItems)): ?>
  <!-- Related items by tag -->
	<div class="btitemRelated">
		<h3><?php echo JText::_("K2_RELATED_ITEMS_BY_TAG"); ?></h3>
		<ul>
			<?php foreach($this->relatedItems as $key=>$item): ?>
			<li class="<?php echo ($key%2) ? "odd" : "even"; ?>">

				<?php if($this->item->params->get('itemRelatedTitle', 1)): ?>
				<a class="itemRelTitle" href="<?php echo $item->link ?>"><?php echo $item->title; ?></a>
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedCategory')): ?>
				<div class="itemRelCat"><?php echo JText::_("K2_IN"); ?> <a href="<?php echo $item->category->link ?>"><?php echo $item->category->name; ?></a></div>
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedAuthor')): ?>
				<div class="itemRelAuthor"><?php echo JText::_("K2_BY"); ?> <a rel="author" href="<?php echo $item->author->link; ?>"><?php echo $item->author->name; ?></a></div>
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedImageSize')): ?>
				<img style="width:<?php echo $item->imageWidth; ?>px;height:auto;" class="itemRelImg" src="<?php echo $item->image; ?>" alt="<?php K2HelperUtilities::cleanHtml($item->title); ?>" />
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedIntrotext')): ?>
				<div class="itemRelIntrotext"><?php echo $item->introtext; ?></div>
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedFulltext')): ?>
				<div class="itemRelFulltext"><?php echo $item->fulltext; ?></div>
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedMedia')): ?>
				<?php if($item->videoType=='embedded'): ?>
				<div class="itemRelMediaEmbedded"><?php echo $item->video; ?></div>
				<?php else: ?>
				<div class="itemRelMedia"><?php echo $item->video; ?></div>
				<?php endif; ?>
				<?php endif; ?>

				<?php if($this->item->params->get('itemRelatedImageGallery')): ?>
				<div class="itemRelImageGallery"><?php echo $item->gallery; ?></div>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>
			<li class="clr"></li>
		</ul>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

	<div class="clr"></div>

  

  <?php if($this->item->params->get('itemImageGallery') && !empty($this->item->gallery)): ?>
  <!-- Item image gallery -->
  <a name="itemImageGalleryAnchor" id="itemImageGalleryAnchor"></a>
  <div class="itemImageGallery">
	  <h3><?php echo JText::_('K2_IMAGE_GALLERY'); ?></h3>
	  <?php echo $this->item->gallery; ?>
  </div>
  <?php endif; ?>

  
  <!-- Plugins: AfterDisplay -->
  <?php echo $this->item->event->AfterDisplay; ?>

  <!-- K2 Plugins: K2AfterDisplay -->
  <?php echo $this->item->event->K2AfterDisplay; ?>

  <?php if($this->item->params->get('itemComments') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1'))): ?>
  <!-- K2 Plugins: K2CommentsBlock -->
  <?php echo $this->item->event->K2CommentsBlock; ?>
  <?php endif; ?>

 <?php if($this->item->params->get('itemComments') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2')) && empty($this->item->event->K2CommentsBlock)): ?>
  <!-- Item comments -->
  <a name="itemCommentsAnchor" id="itemCommentsAnchor"></a>

  <div class="comment-area">
					<div class="container">

	  <?php if($this->item->params->get('commentsFormPosition')=='above' && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2' && K2HelperPermissions::canAddComment($this->item->catid)))): ?>
	  <!-- Item comments form -->
	  <div class="itemCommentsForm">
	  	<?php echo $this->loadTemplate('comments_form'); ?>
	  </div>
	  <?php endif; ?>

	  <?php if($this->item->numOfComments>0 && $this->item->params->get('itemComments') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2'))): ?>
	  <!-- Item user comments -->
	  <h2 class="itemCommentsCounter">
	  	<b><?php echo $this->item->numOfComments; ?></b> <?php echo ($this->item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
	  </h2>

	  <div class="itemCommentsList">
	    <?php foreach ($this->item->comments as $key=>$comment): ?>
	    <div class="<?php echo ($key%2) ? "odd" : "even"; echo (!$this->item->created_by_alias && $comment->userID==$this->item->created_by) ? " authorResponse" : ""; echo($comment->published) ? '':' unpublishedComment'; ?>">

	    	<!--<span class="commentLink">
		    	<a href="<?php echo $this->item->link; ?>#comment<?php echo $comment->id; ?>" name="comment<?php echo $comment->id; ?>" id="comment<?php echo $comment->id; ?>">
		    		<?php echo JText::_('K2_COMMENT_LINK'); ?>
		    	</a>
		    </span>-->

				<?php if($comment->userImage): ?>
				<img src="<?php echo $comment->userImage; ?>" alt="<?php echo JFilterOutput::cleanText($comment->userName); ?>" width="<?php echo $this->item->params->get('commenterImgWidth'); ?>" />
				<?php endif; ?>

				<span class="commentDate">
		    	<?php echo JHTML::_('date', $comment->commentDate, JText::_('K2_DATE_FORMAT_LC2')); ?>
		    </span>

		    <span class="commentAuthorName">
			    <?php echo JText::_('K2_POSTED_BY'); ?>
			    <?php if(!empty($comment->userLink)): ?>
			    <a href="<?php echo JFilterOutput::cleanText($comment->userLink); ?>" title="<?php echo JFilterOutput::cleanText($comment->userName); ?>" target="_blank" rel="nofollow">
			    	<?php echo $comment->userName; ?>
			    </a>
			    <?php else: ?>
			    <?php echo $comment->userName; ?>
			    <?php endif; ?>
		    </span>
<p></p>
		    <p><?php echo $comment->commentText; ?></p>

				<?php if($this->inlineCommentsModeration || ($comment->published && ($this->params->get('commentsReporting')=='1' || ($this->params->get('commentsReporting')=='2' && !$this->user->guest)))): ?>
				<span class="commentToolbar">
					<?php if($this->inlineCommentsModeration): ?>
					<?php if(!$comment->published): ?>
					<a class="commentApproveLink" href="<?php echo JRoute::_('index.php?option=com_k2&view=comments&task=publish&commentID='.$comment->id.'&format=raw')?>"><?php echo JText::_('K2_APPROVE')?></a>
					<?php endif; ?>

					<a class="commentRemoveLink" href="<?php echo JRoute::_('index.php?option=com_k2&view=comments&task=remove&commentID='.$comment->id.'&format=raw')?>"><?php echo JText::_('K2_REMOVE')?></a>
					<?php endif; ?>

					<?php if($comment->published && ($this->params->get('commentsReporting')=='1' || ($this->params->get('commentsReporting')=='2' && !$this->user->guest))): ?>
					<a class="modal" rel="{handler:'iframe',size:{x:560,y:480}}" href="<?php echo JRoute::_('index.php?option=com_k2&view=comments&task=report&commentID='.$comment->id)?>"><?php echo JText::_('K2_REPORT')?></a>
					<?php endif; ?>

					<?php if($comment->reportUserLink): ?>
					<a class="k2ReportUserButton" href="<?php echo $comment->reportUserLink; ?>"><?php echo JText::_('K2_FLAG_AS_SPAMMER'); ?></a>
					<?php endif; ?>

				</span>
				<?php endif; ?>

				<div class="clr"></div>
	    </div>
	    <?php endforeach; ?>
	  </div>

	  <div class="itemCommentsPagination">
	  	<?php echo $this->pagination->getPagesLinks(); ?>
	  	<div class="clr"></div>
	  </div>
		<?php endif; ?>

		<?php if($this->item->params->get('commentsFormPosition')=='below' && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2' && K2HelperPermissions::canAddComment($this->item->catid)))): ?>
	  <!-- Item comments form -->
	  <div class="itemCommentsForm">
	  	<?php echo $this->loadTemplate('comments_form'); ?>
	  </div>
	  <?php endif; ?>

	  <?php $user = JFactory::getUser(); if ($this->item->params->get('comments') == '2' && $user->guest): ?>
	  		<div><?php echo JText::_('K2_LOGIN_TO_POST_COMMENTS'); ?></div>
	  <?php endif; ?>

  </div>
  </div>
  <?php endif; ?>

	<?php if(!JRequest::getCmd('print')): ?>
	<!--<div class="itemBackToTop">
		<a class="k2Anchor" href="<?php echo $this->item->link; ?>#startOfPageId<?php echo JRequest::getInt('id'); ?>">
			<?php echo JText::_('K2_BACK_TO_TOP'); ?>
		</a>
	</div> -->
	<?php endif; ?>
    

</article>
</div>
<!-- End K2 Item Layout -->
<?php if($this->item->params->get('itemNavigation') && !JRequest::getCmd('print') && (isset($this->item->nextLink) || isset($this->item->previousLink))): ?>
  <!-- Item navigation -->
  <div class="blog-nav bg-fs-clr padd-25">
  	<!-- <span class="itemNavigationTitle"><?php echo JText::_('K2_MORE_IN_THIS_CATEGORY'); ?></span> -->
<ul>
		<?php if(isset($this->item->previousLink)): ?>
        <li class="blog-prev brd-wh-clr wh-clr bg-wh-clr-hov fs-clr-hov">
		<a class="itemPrevious" href="<?php echo $this->item->previousLink; ?>">
			<i class="icon-arrow-left"></i> <?php //echo $this->item->previousTitle; ?>
		</a>
        </li>
		<?php endif; ?>
<li class="blog-home brd-wh-clr wh-clr bg-wh-clr-hov fs-clr-hov">
            <a href="#"><i class="icon-menu"></i></a>
        </li>
		<?php if(isset($this->item->nextLink)): ?>
        <li class="blog-next brd-wh-clr wh-clr bg-wh-clr-hov fs-clr-hov">
		<a class="itemNext" href="<?php echo $this->item->nextLink; ?>">
			<?php //echo $this->item->nextTitle; ?> <i class="icon-arrow-right"></i>
		</a>
        </li>
		<?php endif; ?>
</ul>
  </div>
  <?php endif; ?>