<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<!-- Start K2 Tag Layout -->
<div id="k2Container" class="blog-content tagView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?>">

	<?php if($this->params->get('show_page_title')): ?>
	<!-- Page title -->
	<div class="componentheading<?php echo $this->params->get('pageclass_sfx')?>">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</div>
	<?php endif; ?>

	<?php if($this->params->get('tagFeedIcon',1)): ?>
	<!-- RSS feed icon -->
	<div class="k2FeedIcon">
		<a href="<?php echo $this->feed; ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
			<span><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

	<?php if(count($this->items)): ?>
	<?php $item_number = 1; ?>
	<div class="tagItemList">
		<?php foreach($this->items as $item): ?>
		<!-- Start K2 Item Layout -->
		<div class="tagItemView col-1-2 padd-x-10">

			<div class="tagItemHeader">
			  <?php if($item->params->get('tagItemImage',1) && !empty($item->imageXLarge)):?>
				  <!-- Item Image -->
				  <div class="" style="background-image: url('');">
			            <img class="gallery-type-1" src="<?php echo $item->imageXLarge;?>" />
			            <div class="blog-caption transit">
			                <div class="blog-more nd-clr"><a href="<?php echo $this->item->link; ?>"><?php echo JText::_('K2_READ_MORE'); ?></a></div>
			                <div class="blog-icon"><i class="icon-paper"></i></div>
			            </div>
			        </div>
			  <?php endif; ?>
		  </div>

		  <div class="blog-detail padd-25">
			  <?php if($item->params->get('tagItemTitle',1)): ?>
				  <!-- Item title -->
				  <div class="blog-title fs-clr">
				  	<?php if ($item->params->get('tagItemTitleLinked',1)): ?>
						<a href="<?php echo $item->link; ?>">
					  		<?php echo $item->title; ?>
					  	</a>
				  	<?php else: ?>
				  	<?php echo $item->title; ?>
				  	<?php endif; ?>
				  </div>
					<?php if($item->params->get('tagItemDateCreated',1)): ?>
						<!-- Date created -->
						<div class="blog-date marg-bott-25 fs-clr">
							<?php echo JHTML::_('date', $item->created , JText::_('K2_DATE_FORMAT_LC')); ?>
						</div>
					<?php endif; ?>
			  <?php endif; ?>
			  <?php if($item->params->get('tagItemIntroText',1)): ?>
			  <!-- Item introtext -->
			  <div class="post-content">
			  	<?php echo $item->introtext; ?>
			  	<div class="ui-btn ui-btn-medium brd-nd-clr bg-nd-clr-hov marg-right-25 marg-bott-25"><a href="<?php echo $item->link; ?>">Read More</a></div>
			  </div>
			  <?php endif; ?>

			  <div class="clr"></div>
		  </div>
		  
		  <div class="clr"></div>
		  
		  <?php if($item->params->get('tagItemExtraFields',0) && count($item->extra_fields)): ?>
		  <!-- Item extra fields -->  
		  <div class="tagItemExtraFields">
		  	<h4><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></h4>
		  	<ul>
				<?php foreach ($item->extra_fields as $key=>$extraField): ?>
				<?php if($extraField->value != ''): ?>
				<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
					<?php if($extraField->type == 'header'): ?>
					<h4 class="tagItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
					<?php else: ?>
					<span class="tagItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
					<span class="tagItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
					<?php endif; ?>		
				</li>
				<?php endif; ?>
				<?php endforeach; ?>
				</ul>
		    <div class="clr"></div>
		  </div>
		  <?php endif; ?>
		  
			<?php if($item->params->get('tagItemCategory')): ?>
				
			<?php endif; ?>
			
		</div>
		<!-- End K2 Item Layout -->
		
		<?php
		// The below code is needed so the blocks break off properly into columns
			if ($item_number == 2){ 
				?>
				<div class="clr"></div>
				<?php 	
				$item_number = 0;		
			} 
			$item_number++
		?>
		<?php endforeach; ?>
	</div>

	<!-- Pagination -->
	<?php if($this->pagination->getPagesLinks()): ?>
	<div class="k2Pagination">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<div class="clr"></div>
		<?php echo $this->pagination->getPagesCounter(); ?>
	</div>
	<?php endif; ?>

	<?php endif; ?>
	
</div>
<!-- End K2 Tag Layout -->
