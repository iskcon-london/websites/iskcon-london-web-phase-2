Style report NMS  :bomb:
===================
Install Node, NPM and Gulp.

**Installation**

 1. Install Node: https://www.npmjs.com/get-npm
 2. Run npm install in maya directory
 3. That should install Gulp also otherwise install that globally
 4. Commands are "gulp dev" and "gulp prd" without the quotes
 5. Make sure to upload the dist directory
 