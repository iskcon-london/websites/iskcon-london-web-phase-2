(function ($) {
    $(document).ready(function () {
        if ($('#about-accordian').length > 0) {
            $('#about-accordian').accordion({
                heightStyle: "content"
            });
        }
        if ($('#donation_campaigns_list').length > 0) {
            $('#donation_campaigns_list').accordion({
                heightStyle: "content"
            });
        }
        if ($('#slider').length > 0) {
            window.mySwipe = new Swipe(document.getElementById('slider'), {
                startSlide: 0,
                speed: 700,
                auto: 6000,
                draggable: false,
                continuous: true,
                disableScroll: false,
                stopPropagation: false,
                callback: function(index, elem, dir) {},
                transitionEnd: function(index, elem) {}
            });
        }
        //Featherlight lightbox automation via class attribute
        $('.lightbox').each(function (index) {
            var img_src = $(this).attr('src');
            $('<a class="featherlight-' + index + '" href="' + img_src + '" data-featherlight></a> ').insertBefore($(this));
            $(this).removeClass('lightbox').appendTo('.featherlight-' + index);
        });
        //Featherlight lightbox gallery automation via class attribute
        $('.gallery1').each(function (index) {
            var img_src = $(this).attr('src');
            $('<a class="gallery1 gallery1_image-' + index + '" href="' + img_src + '"></a> ').insertBefore($(this));
            $(this).removeClass('gallery1').appendTo('.gallery1_image-' + index);
        });
        $('a.gallery1').featherlightGallery({
            previousIcon: '«',
            nextIcon: '»',
            galleryFadeIn: 300,
            openSpeed: 300
        });
    });
    $(window).load(function(){
         // *only* if we have anchor on the url
        if(window.location.hash) {
            if(window.location.hash==="#main-content"){
                // smooth scroll to the anchor id
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top-150 + 'px'
                }, 1000, 'swing');
            }
        }
    });
})(jQuery);